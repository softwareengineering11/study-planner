package model;


import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import static model.SemesterStudyProfile.saveToFile;

public class User {
    private static final long serialVersionUID = 12349L;

    private String email;
    private String password;
    private String fullName;
    private SemesterStudyProfile semesterStudyProfile;

    public User(String email, String password, String fullName) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;

        JSONObject object = null;
        try {
            System.out.println("trying to json");
            JSONObject userDetails = toJSON();

            System.out.println("writing to file");
            Writer output = null;
            File file = new File("UserFiles/" + email + ".json");
            output = new BufferedWriter(new FileWriter(file));
            output.write(userDetails.toString());
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Cannot json");
        }


    }

    public JSONObject toJSON() {
        JSONObject userDetails = new JSONObject();
        userDetails.put("Email", email);
        userDetails.put("Password", password);
        userDetails.put("Full Name", fullName);

        return userDetails;
    }

    public void setSemesterStudyProfile(SemesterStudyProfile semesterStudyProfile) {
        this.semesterStudyProfile = semesterStudyProfile;
        File file = new File("UserFiles/" + email + ".ser");
        if (saveToFile(file, semesterStudyProfile)) {
            System.out.println("Successfully saved semester study profile.");
        } else {
            System.out.println("Could not save semester study profile.");
        }
    }

    public SemesterStudyProfile getSemesterStudyProfile() {
        return semesterStudyProfile;
    }

    public boolean hasSemesterStudyProfile() {
        return semesterStudyProfile != null;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }
}
