package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
        import java.util.Date;

public class Note implements Serializable{
    private static final long serialVersionUID = 12346L;
    
    private String notes;
    private Date date;

    public Note(String notes, Date date) {
        this.notes = notes;
        this.date = date;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNotes() {

        return notes;
    }

    public Date getDate() {
        return date;
    }
    
    public String getDateStr() {
        DateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
        String str = datef.format(date);
        return str;
    }
}
