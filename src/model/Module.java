package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Module implements Serializable{
    private static final long serialVersionUID = 12344L;
    
    private String name;
    private String code;
    private String description;
    private boolean hasExam;
    private ArrayList<Assessment> assessments;
    
    public Module(String name, String code, String description){
        this.name = name;
        this.code = code;
        this.description = description;
        assessments = new ArrayList<>();
        hasExam = false;
    }
    
    public String getName(){
        return name;
    }
    
    public String getCode(){
        return code;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Assessment> getAssessments() {
        return assessments;
    }
    
    public boolean hasExam(){
        return hasExam;
    }
    
    public void addAssessment(Assessment assessment){
        assessments.add(assessment);
        if(assessment instanceof Exam){
            hasExam = true;
        }
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Exam createExam(Date date, int weight){
        Exam e = new Exam(date,this,weight);
        this.addAssessment(e);
        
        return e;
    }
    public Coursework createCoursework(Date startDate, Date deadline, int weight){
        Coursework cw = new Coursework(startDate,deadline,this,weight);
        this.addAssessment(cw);
        
        return cw;
    }

    @Override
    public String toString() {
        String assessmentsStr = "";
        
        for(Assessment a : assessments){
            assessmentsStr += "\n\t- " + a.toString();
        }
        return name + " (" + code + "): " + description + ", hasExam: " + hasExam + assessmentsStr;
    }
    
    
}
