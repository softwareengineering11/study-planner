/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.*;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author aeq17znu
 */
public class SemesterStudyProfile implements Serializable{
    private static final long serialVersionUID = 12345L;
    
    private int year;
    private String semester;
    private ArrayList<Module> modules;

    public SemesterStudyProfile(int year, String semester) {
        this.year = year;
        this.semester = semester;
        modules = new ArrayList<>();
    }

    public int getYear() {
        return year;
    }

    public String getSemester() {
        return semester;
    }

    public ArrayList<Module> getModules() {
        return modules;
    }

    private void addModule(Module module){
        modules.add(module);
    }
    
    public ArrayList<Assessment> getAssessments() {
        ArrayList<Assessment> allAssessments =  new ArrayList<>();
        for(Module m : modules){
            for(Assessment a : m.getAssessments()){
                allAssessments.add(a);
            }
        }
        Collections.sort(allAssessments, (Assessment a1, Assessment a2) -> a1.getDeadline().compareTo(a2.getDeadline()));
        return allAssessments;
    }
    
   
   public static SemesterStudyProfile importHubFile(File hubFile){
       JSONParser parser = new JSONParser();
       SemesterStudyProfile ssp = null;
       System.out.println("Starting json read");
       try{
           JSONObject obj = (JSONObject) parser.parse(new FileReader(hubFile));
           
           long year = (long) obj.get("Year");
           String semester = (String) obj.get("Semester");
           ssp = new SemesterStudyProfile((int)year, semester);
           
           JSONArray modulesArray = (JSONArray) obj.get("Modules");
           for(Object o1 : modulesArray){
               JSONObject moduleJson = (JSONObject) o1;
               String name = (String) moduleJson.get("Name");
               String code = (String) moduleJson.get("Code");
               String description = (String) moduleJson.get("Description");
               Module module = new Module(name, code, description);
               
               JSONArray assessmentsArray = (JSONArray) moduleJson.get("Assessments");
               for(Object o2 : assessmentsArray){
                   JSONObject assessmentJson = (JSONObject) o2;
                   String type = (String) assessmentJson.get("Type");
                   if(type.equals("e")){
                       String dateStr = (String) assessmentJson.get("date");
                       DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                       Date date = format.parse(dateStr);
                       long weight = (long) assessmentJson.get("weight");
                       module.createExam(date,(int)weight);
                   }
                   else if(type.equals("cw")){
                       String dateStr = (String) assessmentJson.get("startdate");
                       DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                       Date startDate = format.parse(dateStr);
                       dateStr = (String) assessmentJson.get("enddate");
                       Date endDate = format.parse(dateStr);
                       long weight = (long) assessmentJson.get("weight");
                       module.createCoursework(startDate,endDate,(int)weight);
                   }
               }
               ssp.addModule(module);
           }
           System.out.println("Json read complete");
       }
       catch(Exception e){
           System.out.println("exception: " + e);
           e.printStackTrace();
       }
       return ssp;
    }
    
    public static SemesterStudyProfile loadFile(File file) throws IOException, ClassNotFoundException {
       if(!file.exists()){
           System.out.println("Semester study profile file doesn't exist!");
           return null;
       }
        SemesterStudyProfile semester = null;
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fis);
            semester = (SemesterStudyProfile)in.readObject();
            in.close();
        return semester;
    }
    
    public static boolean saveToFile(File file, SemesterStudyProfile semester){
        try{
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(semester);
            out.close();
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
   
    @Override
    public String toString() {
        String modulesStr = "";
        
        for(Module m : modules){
            modulesStr += "\n- " + m.toString();
        }
        return semester + " (Year " + year + ")\nModules:" + modulesStr;
    }
   
   
   
    public static void main(String[] args) {
        SemesterStudyProfile semester = importHubFile(new File("hubfile.json"));
        
        
        Module softeng = semester.getModules().get(0);
        Assessment secw1 = softeng.getAssessments().get(0);
        
        StudyTask t1 = secw1.createTask("Do desktop app",45);
        StudyActivity a1 = t1.createActivity("do model classes", 8, StudyActivity.Type.PROGRAMMING);
        a1.setProgress(6);
        StudyActivity a2 = t1.createActivity("do view classes", 4, StudyActivity.Type.PROGRAMMING);
        a2.setProgress(1);
        t1.createNote("A note. More words. Lots more words. Pretend it's a long note.");
        t1.createNote("A second note. More words. Lots more words. Pretend it's a long note.");
        t1.createNote("A third note. More words. Lots more words. Pretend it's a long note.");
        t1.createNote("A forth note. More words. Lots more words. Pretend it's a long note.");
        
        a1.createNote("A note for and activity. More words. Lots more words. Pretend it's a long note.");
        a1.createNote("A second note. More words. Lots more words. Pretend it's a long note.");
        a1.createNote("A third note. More words. Lots more words. Pretend it's a long note.");
        a1.createNote("A forth note. More words. Lots more words. Pretend it's a long note.");
        
        StudyTask t2 = secw1.createTask("Write report",10);
        t2.addDependency(t1);
        StudyActivity a3 = t2.createActivity("Write introduction", 300, StudyActivity.Type.WRITING);
        a3.setProgress(100);
        StudyTask t21 = secw1.createTask("Final touches",3);
        t21.addDependency(t2);
        t21.addDependency(t1);
        
        Module prog = semester.getModules().get(1);
        Assessment pcw1 = prog.getAssessments().get(0);
        StudyTask t3 = pcw1.createTask("Do cw",30);
        StudyActivity a4 = t3.createActivity("do cw", 3, StudyActivity.Type.PROGRAMMING);
        a4.setProgress(3);
        
        Module dsa = semester.getModules().get(2);
        Assessment dsacw1 = dsa.getAssessments().get(0);
        StudyTask t4 = dsacw1.createTask("Do cw",15);
        StudyActivity a5 = t4.createActivity("do cw", 8, StudyActivity.Type.PROGRAMMING);
        a5.setProgress(7);
        
        Assessment dsacw2 = dsa.getAssessments().get(1);
        StudyTask t5 = dsacw2.createTask("Do cw",15);
        StudyActivity a6 = t5.createActivity("do cw", 10, StudyActivity.Type.PROGRAMMING);
        a6.setProgress(10);
        
        System.out.println(semester.getAssessments().size());
        
        System.out.println(semester);
        /*try{
            FileOutputStream fos = new FileOutputStream("semesterProfile.ser");
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(semester);
            out.close();
        }
        catch(Exception e){
            System.out.println("Exception = "+e);
        }*/
        
        System.out.println("\n****** Saving file ******");
        System.out.println("Saved? " + saveToFile(new File("semesterProfile.ser"), semester));
        
        System.out.println("\n****** Loading file ******");
        SemesterStudyProfile sFromFile = null;
        try {
            sFromFile = loadFile(new File("semesterProfile.ser"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Loaded:\n" + sFromFile);
        
    }
    
}
