/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * @author aeq17znu
 */
public class Coursework extends Assessment implements Serializable{
    private static final long serialVersionUID = 12341L;
    
    public Coursework(Date startDate, Date deadline, Module module, int weight){
        super(module, weight);
        String month = new SimpleDateFormat("MMMM",Locale.UK).format(deadline);
        name = module.getName() + " Coursework (due " + month + ")";
        this.startDate = startDate;
        this.deadline = deadline;
    }
}
