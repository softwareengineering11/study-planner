/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author aeq17znu
 */
public class Exam extends Assessment implements Serializable{
    private static final long serialVersionUID = 12342L;
    
    public Exam(Date date, Module module, int weight){
        super(module, weight);
        name = module.getName() + " Exam";
        startDate = deadline = date;
    }  
    
}
