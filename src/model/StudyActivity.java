package model;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;

public class StudyActivity implements Serializable{
    private static final long serialVersionUID = 12347L;

    //Class attributes
    private String name;
    private boolean hasBeenDone; //Tells whether the activity is done (TRUE).
    private ArrayList<StudyTask> tasks; //This will store which task the activity is part of?
    private Duration timeSpent;
    private int target;
    private float progress;
    private Type type;
    private ArrayList<Note> notes;

   public enum Type { READING("pages"), WRITING("words"), STUDYING("hours"), PROGRAMMING("requirements"); 

        private final String targetUnits;
        
        Type(String units){ targetUnits = units; }
    
        public String getUnits(){ return targetUnits; }
    @Override
        public String toString(){
            String str = "";
            if(null!=this)switch (this) {
                case PROGRAMMING:
                    str = "Programming";
                    break;
                case READING:
                    str = "Reading";
                    break;
                case WRITING:
                    str = "Writing";
                    break;
                case STUDYING:
                    str = "Studying";
                    break;
                default:
                    break;
            }
        return str;
        }
    }

    //Constructor
    public StudyActivity(String name, StudyTask s, int target, Type type){
        this.tasks = new ArrayList<>();
        this.notes = new ArrayList<>();
        this.name = name;
        this.tasks.add(s);
        this.target = target;
        this.type = type;
        this.progress = 0;
    }
    
    /*========================= ACCESSOR METHODS =============================*/
    public boolean isHasBeenDone() {
        return hasBeenDone;
    }

    public ArrayList<StudyTask> getTasks() {
        return tasks;
    }

    public Duration getTimeSpent() {
        return timeSpent;
    }
    
    public String getName(){
        return name;
    }
    
    public int getTarget(){
        return target;
    }
    
    public float getProgress(){
        return progress;
    }
    
    public ArrayList<Note> getNotes(){
        return notes;
    }
    
    public Type getType(){
        return type;
    }
    
    public String getTypeStr(){
        return type.toString();
    }
    
    /*=========================== MUTATOR METHODS ============================*/
    
    public void setName(String name){
        this.name = name;
    }
    
    public boolean setProgress(int unitsCompleted) {
        if(unitsCompleted > target){
            return false;
        }
        
        this.progress = (float)unitsCompleted/target;
        return true;
    }
    public void setTarget(int target) {
        int completed = (int)(progress*this.target);
        this.target = target;
        this.progress = (float)completed/target;
    }

    public void setTimeSpent(Duration timeSpent) {
        this.timeSpent = timeSpent;
    }
    
    public boolean setType(String typeStr){
        System.out.println(typeStr);
        for(Type t : Type.values()){
            System.out.println(typeStr + " = " + t.toString() + "? "
                    + typeStr.equals(t.toString()));
            if(typeStr.equals(t.toString())){
                this.type = t;
                return true;
            }
        }
        return false;
    }
    
    public void addTask(StudyTask t){ tasks.add(t); }
    public boolean removeTask(StudyTask t) {
        return tasks.remove(t);
    }
    
    public void addNote(Note n){ notes.add(n); }
    public boolean removeNote(String noteTxt) {
        for(Note n : notes){
            if(n.getNotes().equals(noteTxt)){
                notes.remove(n);
                return true;
            }
        }
        return false;
    }
    
    
    
    public Note createNote(String note){
        Date today = new Date();
        Note n = new Note(note,today);
        this.addNote(n);
        
        return n;
    }
    
    @Override
    public String toString() {
        return "Activity: " + name + " (" + type + "): " + (int)(progress*target) 
                + "/" + target + " " + type.getUnits() + " completed (" + (int)(progress*100) + "%)";
    }
   
    
}

//Make StudyActivity abstract
//Types of activities:
//      Reading - Number of pages read
//      Writing - Word Count
//      Studying - Time spent
//      Programming - assignment requirements completed
