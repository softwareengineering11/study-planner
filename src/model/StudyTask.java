package model;

import java.io.Serializable;
import java.time.Duration;
import java.util.*;

public class StudyTask implements Serializable{
    private static final long serialVersionUID = 12348L;
    
    private String name;
    private String description;
    private Assessment assessment;
    private Duration timeToBeSpent;
    private ArrayList<StudyTask> requiredTasks;
    private ArrayList<Note> notes;
    
    private float progress;
    private ArrayList<StudyActivity> activities;
    
    public StudyTask(String name, Assessment assessment, int daysToBeSpent){
        this.name = name;
        this.assessment = assessment;
        this.timeToBeSpent = Duration.ofDays((long)daysToBeSpent);
        this.activities = new ArrayList<>();
        this.notes = new ArrayList<>();
        this.requiredTasks = new ArrayList<>();
    }
    
    public String getName(){
        return name;
    }
    
    public float getProgress(){
        int numActiv = activities.size();
        progress = 0;
        for(StudyActivity a : activities){
            progress += a.getProgress()/numActiv;
        }
        return progress;
    }

    public String getDescription() {
        return description;
    }

    public Assessment getAssessment() {
        return assessment;
    }

    public Duration getTimeToBeSpent() {
        return timeToBeSpent;
    }
    
    public int getDaysToBeSpent() {
        return (int)timeToBeSpent.toDays();
    }

    public ArrayList<StudyTask> getRequiredTasks() {
        return requiredTasks;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public ArrayList<StudyActivity> getActivities() {
        return activities;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setTimeToBeSpend(Duration timeToBeSpent){
        this.timeToBeSpent = timeToBeSpent;
    }
    
    public void addActivity(StudyActivity activity){
        activities.add(activity);
    }
    
    public void addDependency(StudyTask otherTask){
        requiredTasks.add(otherTask);
    }
    public boolean removeRequiredTask(StudyTask t) {
        return requiredTasks.remove(t);
    }
    
    public void addNote(Note note){
        notes.add(note);
    }
    public boolean removeNote(String noteTxt) {
        for(Note n : notes){
            if(n.getNotes().equals(noteTxt)){
                notes.remove(n);
                return true;
            }
        }
        return false;
    }
    
    public void removeActivity(StudyActivity activity){
        activities.remove(activity);
    }
    
    public int numOfActivities(){
        return activities.size();
    }
    
    public int numOfCompletedActivities(){
        int count = 0;
        for(StudyActivity a : activities){
//            if(a.hasBeenDone()){
//                count++;
//            }
        }
        return count;
    }
    
    public StudyActivity createActivity(String name, int target, StudyActivity.Type type){
        StudyActivity a = new StudyActivity(name,this, target, type);
        this.addActivity(a);
        
        return a;
    }
    public Note createNote(String note){
        Date today = new Date();
        Note n = new Note(note,today);
        this.addNote(n);
        
        return n;
    }

    @Override
    public String toString() {
        String activStr = "";
        for(StudyActivity a : activities){
            activStr += "\n\t\t\t- " + a.toString();
        }
        return "Task: " + name + " (progress: " + (int)(getProgress()*100) + "%)" + activStr;
    }
    
    
}
