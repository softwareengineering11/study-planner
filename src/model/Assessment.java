/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author aeq17znu
 */
public abstract class Assessment implements Serializable{
    private static final long serialVersionUID = 12340L;
    
    protected Date startDate;
    protected Date deadline;
    protected String name;
    protected String description;
    protected int weight;
    protected Module module;
    protected ArrayList<StudyTask> tasks;
    protected float progress;
    
    public Assessment(Module module, int weight){
        this.module = module;
        this.weight = weight;
        this.tasks = new ArrayList<>();
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getDeadline() {
        return deadline;
    }
    
    public String getStartDateStr(){
        DateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
        String str = datef.format(startDate);
        return str;
    }
    
    public String getDeadlineStr(){
        DateFormat datef = new SimpleDateFormat("dd/MM/yyyy");
        String str = datef.format(deadline);
        return str;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<StudyTask> getTasks() {
        return tasks;
    }

    public int getWeight() {
        return weight;
    }

    public Module getModule() {
        return module;
    }
    
    public float getProgress(){
        int numTasks = tasks.size();
        progress = 0;
        for(StudyTask t : tasks){
            progress += t.getProgress()/numTasks;
        }
        return progress;
    }

    public void addTask(StudyTask task){
        tasks.add(task);
    }
    
    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public void setName(String eventName) {
        this.name = eventName;
    }

    public void setDescription(String eventDescription) {
        this.description = eventDescription;
    }

    @Override
    public String toString() {
        DateFormat datef = new SimpleDateFormat("dd MMM yyyy");
        String tasksStr = "";
        for(StudyTask t : tasks){
            tasksStr += "\n\t\t- " + t.toString();
        }
        
        return name + " (" + weight + "%) - deadline: " + datef.format(deadline) 
                + " (progress: " + (int)(getProgress()*100) + "%)" + tasksStr;
    }
    
    public StudyTask createTask(String name, int daysToBeSpent){
        StudyTask t = new StudyTask(name,this,daysToBeSpent);
        this.addTask(t);
        
        return t;
    }
    
}
