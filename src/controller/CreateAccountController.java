package controller;

import java.io.File;
import java.util.regex.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.User;
import view.CreateAccountForm;
import view.SceneManager;

public class CreateAccountController {

    private User user;
    CreateAccountForm view;
    private String email;
    private String pw;
    private String fullName;
    private EventHandler<ActionEvent> backHandler;

    private EventHandler<ActionEvent> registerSuccessHandler;

    public CreateAccountController() {
        view = new CreateAccountForm();
        view.setCreateHandler(event -> {
            email = view.getEmail();
            pw = view.getPassword();
            fullName = view.getFullName();

            if(validates(email,pw,fullName)) {
                try {


                    new File("UserFiles").mkdir(); // try to make userfile folder.
                    if(new File("UserFiles/"+email+".json").exists()) {
                        System.out.println("User Exists");
                    }
                    else{
                        System.out.println("New user");
                        user = new User(view.getEmail(), view.getPassword(), view.getFullName());
                        if(registerSuccessHandler!=null) {registerSuccessHandler.handle(event);}
                        view.setEmpty();
                        //sceneManager.changeScene("Dashboard");
                    }



                    //Change scene

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Unable to make an Account");
                    // Reload Create account scene.
                }

            }else{
                view.cantRegister();
            }


        });

        view.setBackHandler(event -> {
            if(backHandler != null) backHandler.handle(event);
        });
    }

    public CreateAccountForm getView() {
        return view;
    }

    public void setBackHandler(EventHandler<ActionEvent> backHandler) {
        this.backHandler = backHandler;
    }

    public boolean validates(String email,String pw,String fullname){
        String pattern = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        //check for unicode chars too ?

        if(email.matches(pattern) &&pw.length()<=20 && fullname.length()<=20
                && pw.length()>=3 && fullname.length()>=4){ return true;}

        return false;
    }

    public void setRegisterSuccessHandler(EventHandler<ActionEvent> registerSuccessHandler) {
        this.registerSuccessHandler = registerSuccessHandler;
    }

    public User getUser() {
        return user;
    }
}
