package controller;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javafx.scene.Node;
import model.Assessment;
import model.SemesterStudyProfile;
import view.deadlines.DeadlinesPane;
import view.deadlines.DeadlineInfo;

/**
 * Deadlines Controller
 * 
 * Controller for DeadlinesPane view. Sets the list deadlines for each category
 * according to the SemesterStudyProfile of the user
 * 
 */
public class DeadlinesController {
    private SemesterStudyProfile profile;

    private DashboardController parent;

    private DeadlinesPane view;
    
    public DeadlinesController(DashboardController parent){
        this.view = new DeadlinesPane(parent);
        this.parent = parent;
    }

    public void setProfile(SemesterStudyProfile profile) {
        this.profile = profile;
        this.view.clearView();
        updateView();
    }
    
    public void updateView(){
        if(profile == null) return;
        Date today = Calendar.getInstance().getTime();
        // For each assessment in the study profile, decide which category(s) it
        // should be shown under
        for(Assessment a : profile.getAssessments()){
            Date deadline = a.getDeadline();
            long diffTime = deadline.getTime() - today.getTime();
            long diffDays = diffTime / (1000 * 60 * 60 * 24);

            String name = a.getName();
            String dateStr = a.getDeadlineStr();
            float progress = a.getProgress();
            
            // Deadline has passed and has not been completed yet -> missed
            if(deadline.before(today) && a.getProgress()<1.0){
                this.view.addToMissed(new DeadlineInfo(name,dateStr,progress));
            }
            // Progress is 100% -> completed
            if(a.getProgress()==1.0){
                this.view.addToCompleted(new DeadlineInfo(name,dateStr,progress));
            }
            // Progress is more than 0%, but less than 100% -> in progress
            if(a.getProgress()>0.0 && a.getProgress()<1.0){
                this.view.addToInProgress(new DeadlineInfo(name,dateStr,progress));
            }
            // Deadline hasn't passed yet -> upsoming
            if(!deadline.before(today)){
                /* If deadline is in less than a month it will always be added to upcoming
                 * If it is further away, it will be added is there are less than 4 deadlines
                 * so far on the list (to avoid overwhelming) */
                if(diffDays<31 || this.view.getUpcomingSize()<4) {
                    this.view.addToUpcoming(new DeadlineInfo(name,dateStr,progress));
                }
            }
        }
        this.view.createView();
    }

    public Node getView() {
        return view;
    }
}
