package controller;

import model.SemesterStudyProfile;
import view.ganttchart.GanttChart;
import view.ganttchart.GanttChartGroup;
import view.ganttchart.GanttChartTask;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

public class GanttChartController {

    private GanttChart view;
    private SemesterStudyProfile profile;

    public GanttChartController(SemesterStudyProfile model){
        view = new GanttChart();
        profile = model;
    }

    public GanttChart getView() {
        return view;
    }

    public void setProfile(SemesterStudyProfile profile) {
        this.profile = profile;
        createGanttModel();
    }

    /**
     * Notifies the renderer that the model has changed.
     * This method will recreate the viewmodel
     */
    public void refreshModel(){
        this.view.clearGroups();
        this.view.invalidateCache();
        createGanttModel();
        this.view.requestRender();
    }

    /**
     * Creates the viewmodel from the model and passes it to the renderer
     */
    private void createGanttModel(){
        if(profile == null) return;

        profile.getAssessments().forEach(assessment -> {
            GanttChartGroup group = new GanttChartGroup(assessment.getName(), LocalDateTime.ofInstant(Instant.ofEpochMilli(assessment.getStartDate().getTime()), ZoneId.systemDefault()));
            assessment.getTasks().forEach(studyTask -> group.addTasks(new GanttChartTask(studyTask.getName(), studyTask.getTimeToBeSpent())));
            assessment.getTasks().forEach(studyTask -> {
                GanttChartTask ganttTask = group.getTasks().stream().filter(ganttChartTask -> ganttChartTask.getName().equals(studyTask.getName())).findAny().get();
                if(ganttTask == null) return;
                System.out.println(Arrays.toString(studyTask.getRequiredTasks().toArray()));
                studyTask.getRequiredTasks().forEach(dependency -> {
                    GanttChartTask dependencyTask = group.getTasks().stream().filter(ganttChartTask -> ganttChartTask.getName().equals(dependency.getName())).findAny().get();
                    group.addDependency(ganttTask, dependencyTask);
                });
            });
            this.view.addGroup(group);
        });
    }
}
