/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import model.Assessment;
import model.Note;
import model.SemesterStudyProfile;
import model.StudyActivity;
import model.StudyTask;
import view.AlertBox;
import view.ConfirmBox;
import view.assessment.TaskInfo;
import view.assessment.TaskInfo.*;
import view.assessment.ActivityView;

/**
 * Activity Controller
 * 
 * Controller for Activity view. Gets the information of a specific activity
 * from the SemesterStudyProfile of the user and sends the information to the view
 * 
 */
public class ActivityController {
    private SemesterStudyProfile semester;
    private File file;
    private StudyActivity studyactivity;
    private final DashboardController dashboard;
    
    private final ActivityView view;
    
    public ActivityController(DashboardController dashboard,ActivityView view, String assessmentName, 
            String taskName, String activityName){
        this.view = view;
        //file = new File("semesterProfile.ser");    //TEST FILE
        //Loading semester object
        this.dashboard = dashboard;
        semester = dashboard.getProfile();
        
        // Sending information to the view
        search:
        for(Assessment a : semester.getAssessments()){
            if(a.getName().equals(assessmentName)){
                for(StudyTask t : a.getTasks()){
                    if(t.getName().equals(taskName)){
                        for(StudyActivity act : t.getActivities()){
                            studyactivity = act;
                            ActivityInfo activity = activToActivityInfo(act);
                            activity.setAssessment(a.getName());
                            if(act.getName().equals(activityName)){
                                for(Note n : act.getNotes()){
                                    activity.addNote(new NoteInfo(n.getNotes(), n.getDateStr()));
                                }
                                for(StudyTask t2 : act.getTasks()){
                                    activity.addTask(taskToTaskInfo(t2));
                                }
                                view.setActivity(activity);
                                break search;
                            }
                        }
                    }
                }
            }
        }
    }
    
    public boolean setName(String newName) {
        boolean success = true;
        // Can't set new name if it is empty
        if(newName==null || newName.isEmpty()){
            success = false;
            AlertBox.display("Invalid entry","You must write a name for the task.");
        }
        // Can't set new name if there is another activity with that name
        if(!newName.equals(studyactivity.getName())){
            for(StudyTask t : studyactivity.getTasks()){
                for(StudyActivity a : t.getActivities()){
                    if(a.getName().equals(newName)){
                    success = false;
                    AlertBox.display("Invalid entry", "There's already an "
                         + "activity with that name. Try a different name.");
                    break;
                }
            }      
            }
            if(success){
                studyactivity.setName(newName);
                dashboard.saveProfile();
                dashboard.updateControllers();
            }
        } 
        return success;
    }
    
    public boolean setCompleted(int completed) {
        boolean success = true;
        // Can't set negative numbers
        if(completed<0){
            success = false;
            AlertBox.display("Invalid entry", "You must enter a number superior or "
                    + "equal to zero.");
        }
        // Can't set a number bigger than the target
        else{
            if(completed>studyactivity.getTarget()){
                success = false;
                AlertBox.display("Invalid entry", "You can not set a number superior"
                        + " to the target.");
            }
        }
        if(success){
            studyactivity.setProgress(completed);
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        return success;
    }
    
    public boolean setTarget(int target) {
        boolean success = true;
        // Can't set negative numbers or 0
        if(target<=0){
            success = false;
            AlertBox.display("Invalid entry", "You must enter a number superior "
                    + "to zero.");
        }
        else{
            // Can't set a number smaller than what's been completed already
            if(target<studyactivity.getProgress()*studyactivity.getTarget()){
                success = false;
                AlertBox.display("Invalid entry", "You can not set a number inferior"
                        + " to the numbers of completed units.");
            }
        }
        if(success){
            studyactivity.setTarget(target);
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        return success;
    }
    
    public boolean setType(String type) {
        boolean success = studyactivity.setType(type);
        if(success){
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        else{
            AlertBox.display("Error!", "Something went wrong.");
        }
        return success;
    }
    
    public NoteInfo addNote(String noteTxt) {
        for(Note n : studyactivity.getNotes()){
            // Can't add a note if there is one with the exact same content
            if(n.getNotes().equals(noteTxt)){
                AlertBox.display("Invalid entry", "You've already entered a note with that same content.");
                return null;
            }
        }
        Note note = new Note(noteTxt, new Date());
        NoteInfo noteInfo = new NoteInfo(note.getNotes(), note.getDateStr());
        studyactivity.addNote(note);
        dashboard.saveProfile();
        dashboard.updateControllers();
        return noteInfo;
    }
    
    public boolean removeNote(String noteTxt) {
        // Prompts user to confirm they want to remove a note
        boolean remove = ConfirmBox.display("Remove note","Are you sure you want to delete this note?");
        if(remove){
            remove = studyactivity.removeNote(noteTxt);
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        return remove;
    }
    
    public TaskInfo addTask(String taskName) {
        for(StudyTask t : studyactivity.getTasks().get(0).getAssessment().getTasks()){
            if(t.getName().equals(taskName)){
                studyactivity.addTask(t);
                t.addActivity(studyactivity);
                dashboard.saveProfile();
                dashboard.updateControllers();
                return taskToTaskInfo(t);
            }
        }
        AlertBox.display("Error!", "Something went wrong. Task was not added.");
        return null;
    }
    
    public boolean removeTask(String taskName) {
        boolean remove = false;
        // Prompts user to confirm they want to remove a task
        if(ConfirmBox.display("Remove task","Are you sure you want to remove task \"" 
                + taskName + "\" from this activity?")){
            if(studyactivity.getTasks().size()>1){
                for(StudyTask t : studyactivity.getTasks()){
                    if(t.getName().equals(taskName)){
                        remove = studyactivity.removeTask(t);
                        t.removeActivity(studyactivity);
                        dashboard.saveProfile();
                        dashboard.updateControllers();
                        return remove;
                    }
                }
                dashboard.saveProfile();
                dashboard.updateControllers();
           }
            // Can't remove a task if it is the only task associated with the activity
           else{
               AlertBox.display("Cannot remove task", "An activity must always have at "
                    + "least one task associated. Add another task, before removing this one.");
               return remove;
           }
        }
       return remove;
    }

    
    public ArrayList<String> getTaskOptions(){
        ArrayList<String> taskOptions = new ArrayList<>();
        for(StudyTask t : studyactivity.getTasks().get(0).getAssessment().getTasks()){
            if(!studyactivity.getTasks().contains(t)){
                taskOptions.add(t.getName());
            }
        }
        return taskOptions;
    }
    
    public void deleteActivity() {
        for(StudyTask t : studyactivity.getTasks()){
            t.getActivities().remove(studyactivity);
        }
        dashboard.saveProfile();
        dashboard.updateControllers();
    }
    
    // Converts a StudyActivity to an ActivityInfo object
    public static ActivityInfo activToActivityInfo(StudyActivity a){
        ActivityInfo activ = new ActivityInfo(a.getName(),
                                a.getTypeStr(), a.getProgress(),a.getTarget(),
                                (int)(a.getProgress()*a.getTarget()),
                                a.getType().getUnits());
        return activ;
    }
    // Converts a Note to a NoteInfo object
    public static TaskInfo taskToTaskInfo(StudyTask t){
        TaskInfo task = new TaskInfo(t.getName(), t.getDescription(), 
                                     t.getAssessment().getName(),
                                     t.getDaysToBeSpent(), t.getProgress());
        return task;
    }

    
}
