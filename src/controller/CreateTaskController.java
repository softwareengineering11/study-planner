/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;

import model.Assessment;
import model.Module;
import model.SemesterStudyProfile;
import model.StudyTask;
import view.CreateTask;

/**
 * @author heb16acu
 */
public class CreateTaskController {

    private CreateTask view;
    private Module module;
    private Assessment assessment;
    private StudyTask task;
    private Runnable onTaskAdded;
    private SemesterStudyProfile profile;
    private ArrayList<String> moduleNames;
    private ArrayList<String> assessmentNames;
    private ArrayList<String> taskNames;
    
    public CreateTaskController(CreateTask view, SemesterStudyProfile profile, DashboardController dashController) {
        this.profile = profile;
        this.view = view;

        moduleNames = new ArrayList<>();
        assessmentNames = new ArrayList<>();
        taskNames = new ArrayList<>();

        for (int i = 0; i < profile.getModules().size(); i++) {
            moduleNames.add(profile.getModules().get(i).getName());

        }


        view.setModules(moduleNames);

        //view.setDependances(li);

        this.view.setOnClickModule(() -> {
            assessmentNames.clear();
            taskNames.clear();
            for (int i = 0; i < profile.getModules().size(); i++) {

                if (profile.getModules().get(i).getName().equals(view.getModuleChange())) {
                    module = profile.getModules().get(i);
                    for (int j = 0; j < profile.getModules().get(i).getAssessments().size() ; j++) {
                        assessmentNames.add(profile.getModules().get(i).getAssessments().get(j).getName());


                    }
                }
            }
            view.setAssessments(assessmentNames);

        });

        this.view.setOnClickAssess(() -> {
            taskNames.clear();

            for (int i = 0; i < module.getAssessments().size(); i++) {
                if (module.getAssessments().get(i).getName().equals(view.getAssessChange())) {
                    this.assessment = module.getAssessments().get(i);
                    for (int j = 0; j < module.getAssessments().get(i).getTasks().size(); j++) {
                        taskNames.add(module.getAssessments().get(i).getTasks().get(j).getName());

                    }
                }
            }
            view.setDependances(taskNames);
        });

        this.view.setOnSubmit(() -> {

            if (!view.getDependances().getSelectionModel().isEmpty()) {
                for (int j = 0; j < assessment.getTasks().size(); j++) {
                    if (assessment.getTasks().get(j).getName().equals(view.getDependances().getSelectionModel().getSelectedItem().toString())) {
                        StudyTask task = assessment.createTask(view.getTaskNameTxt().getText(), Integer.parseInt(view.getDuration().getText()));
                        task.addDependency(assessment.getTasks().get(j));
                        task.setDescription(view.getDescription().getText());
                    }
                }

            } else {
                assessment.createTask(view.getTaskNameTxt().getText(), Integer.parseInt(view.getDuration().getText())).setDescription(view.getDescription().getText());
            }

            if (profile.getAssessments().get(0).getTasks().size() > 0) {
                System.out.println(Arrays.toString(profile.getAssessments().get(0).getTasks().get(1).getRequiredTasks().toArray()));
            }
            dashController.saveProfile();
            if(onTaskAdded != null) onTaskAdded.run();
        });

    }

    public CreateTask getView() {
        return view;
    }

    public void setOnTaskAdded(Runnable onTaskAdded) {
        this.onTaskAdded = onTaskAdded;
    }
    
    public void setAssessmentPrefilled(String module_, String assessment_){
        for (Module m : profile.getModules()) {
            if (m.getName().equals(module_)) {
                module = m;
                for (Assessment a : m.getAssessments()) {
                    if(a.getName().equals(assessment_)){
                        assessment = a;
                        taskNames.clear();
                        for(StudyTask t : a.getTasks()){
                            taskNames.add(t.getName());
                        }
                    }
                }
            }
        }
        view.setDependances(taskNames);
        view.setAssessmentPrefilled(module_,assessment_);
    }
}
