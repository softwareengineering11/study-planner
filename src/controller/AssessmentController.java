/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.ActivityController.taskToTaskInfo;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import model.Assessment;
import model.Exam;
import model.SemesterStudyProfile;
import model.StudyTask;
import view.AlertBox;
import view.ConfirmBox;
import view.assessment.AssessmentView;

/**
 * Assessment Controller
 * 
 * Controller for Assessment view. Gets the information of a specific assessment
 * from the SemesterStudyProfile of the user and sends the information to the view
 * 
 */
public class AssessmentController {
    private SemesterStudyProfile semester;
    private Assessment assessment;
    private File file;
    
    private final AssessmentView view;
    private final DashboardController dashboard;
    
    public AssessmentController(DashboardController dashboard, AssessmentView view, String assessmentName){
        this.view = view;
        this.dashboard = dashboard;
        //this.file = new File("semesterProfile.ser");   //TEST FILE
        //Loading semester object
        semester = dashboard.getProfile();
        
        // Sending information to the view
        for(Assessment a : semester.getAssessments()){
            if(a.getName().equals(assessmentName)){
                assessment = a;
                this.view.setName(assessmentName);
                this.view.setDescription(a.getDescription());
                this.view.setModule(a.getModule().getName());
                this.view.setModuleCode(a.getModule().getCode());
                this.view.setStartDate(a.getStartDateStr());
                this.view.setDeadline(a.getDeadlineStr());
                this.view.setWeight(a.getWeight());
                this.view.setProgress(a.getProgress());
                if(a instanceof Exam) this.view.setType("Exam");
                else this.view.setType("Coursework");
                for(StudyTask t : a.getTasks()){
                     this.view.addTask(taskToTaskInfo(t));
                }
                break;
            }
        }
    }
    
    public boolean setName(String newName){
        boolean success = true;
        // Can't set new name if it is empty
        if(newName==null || newName.isEmpty()){
            success = false;
            AlertBox.display("Invalid entry","You must write a name for the assessment.");
        }
        if(!newName.equals(assessment.getName())){
            for(Assessment a : semester.getAssessments()){
                // Can't set new name if there is another assessment with that name
                if(a.getName().equals(newName)){
                    success = false;
                    AlertBox.display("Invalid entry", "There's already an "
                         + "assessment with that name. Try a different name.");
                    break;
                }
            }
            // If all's been successful so far, set the new name, save to file
            // and update controllers
            if(success){
                assessment.setName(newName);
                dashboard.saveProfile();
                dashboard.updateControllers();
            }
        } 
        return success;
    }
    
    public void setDescription(String newDesc){
        assessment.setDescription(newDesc);
        dashboard.saveProfile();
        dashboard.updateControllers();
    }

    public boolean setStartDate(LocalDate localDate) {
        boolean success=false;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateStr = localDate.format(formatter);
        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
            // Can't set new start date if it is after the deadline
            if(date.after(assessment.getDeadline())){
                AlertBox.display("Invalid entry",
                        "The start date cannot be after the deadline.");
            }
            else {
                assessment.setStartDate(date);
                dashboard.saveProfile();
                dashboard.updateControllers();
                success = true;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
            AlertBox.display("Error","Error! " + ex);
        }
        return success;
    }
    
    public boolean setDeadline(LocalDate localDate) {
        boolean success=false;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateStr = localDate.format(formatter);
        System.out.println(dateStr);
        try {
            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
            Date today = new Date();
            // Can't set new end date if it is before the start date
            if(date.before(assessment.getStartDate())){
                AlertBox.display("Invalid entry",
                        "The deadline date cannot be before the start date.");
            }
            // Prompts user to confirm if they set a new deadline as a past date
            else if(date.before(today)){
                success = ConfirmBox.display("Confirmation required", "You set the deadline to a past date. Are you sure you want to continue?");
            } else success = true;
            if(success) {
                assessment.setDeadline(date);
                dashboard.saveProfile();
                dashboard.updateControllers();
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
            AlertBox.display("Error", "Error! " + ex);
        }
        return success;
    }
}
