package controller;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import model.Assessment;
import model.Module;
import view.SceneManager;
import view.dashboard.DashboardPane;
import view.module.ModuleAssessment;
import view.module.ModuleInformation;
import view.module.ModuleInformation.AssessmentButtonClickHandler;
import view.module.ModulePane;


public class ModuleController {
    private ArrayList<Module> modules; //Model object
    private ModulePane firstView; //View object for the first screen.
    private ModuleInformation secondView;
    private EventHandler<ActionEvent> buttonHandler;
    private EventHandler<ActionEvent> backButtonHandler;
    private DashboardPane parentView;
    
    public ModuleController(DashboardPane parentView, ArrayList<Module> modules){
        this.modules = modules;
        this.firstView = new ModulePane();
        secondView = new ModuleInformation();
        this.parentView = parentView;
        
        
        this.firstView.setModuleClickHandler(code -> {
            secondViewInfo(code);
        });
        
        this.secondView.setBackClickHandler(event -> {
            parentView.setContentView(firstView);
            
        });
    }
    
    public String getModuleName(int index){
        return modules.get(index).getName();
    }
    
    public String getModuleCode(int index){
        return modules.get(index).getCode();
    }
    
    public String getModuleDescription(int index){
        return modules.get(index).getDescription();
    }
    
    public void updateView(int index){
        firstView.addModule(this.getModuleName(index), this.getModuleCode(index));
    }
    
    public void setAssessmentClickHandler(AssessmentButtonClickHandler asssessmentHandler){
        this.secondView.setAssessmentClickHandler(asssessmentHandler);
    }

    //Returns the ModulePane view (First view)
    public Node getModulePaneView() {
        return firstView;
    }
    
    public void secondViewInfo(String code){
        String moduleName = "";
            String moduleDescription = "";
            secondView.clearBorderPane();
            for(Module module : modules){
                if(module.getCode().equals(code)){
                    moduleName = module.getName(); //Gets the module name
                    moduleDescription = module.getDescription(); //Gets the module description
                    for(Assessment assessment : module.getAssessments()){
                        String name = assessment.getName();
                        String startDate = assessment.getStartDateStr();
                        String deadline = assessment.getDeadlineStr();
                        int weight = assessment.getWeight();
                        ModuleAssessment a = new ModuleAssessment(name, startDate, deadline, weight);
                        secondView.addAssessment(a);
                    }
                    break;
                }
            }
            secondView.addInformation(moduleName, code, moduleDescription);
            parentView.setContentView(secondView);
    }
    
    
}
