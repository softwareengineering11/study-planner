package controller;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import model.SemesterStudyProfile;
import model.User;
import view.SceneManager;
import view.assessment.AssessmentView;
import view.dashboard.DashboardPane;
import view.deadlines.DeadlinesPane;

import java.io.File;
import java.io.IOException;

import view.CreateTask;
import model.Module;
import view.module.ModulePane;


public class DashboardController {

    private User user;
    private SemesterStudyProfile studyProfile;

    private DashboardPane view;
    private ModuleController moduleController;

    private DashboardImportController importController;

    private DeadlinesController deadlinesController;

    private GanttChartController ganttController;

    private CreateTaskController createTaskController;

    private Runnable onLogout;

    public DashboardController(Window owner, DashboardPane view) {
        this.view = view;

        deadlinesController = new DeadlinesController(this);
        ganttController = new GanttChartController(studyProfile);

        this.view.setView("Deadlines", deadlinesController.getView());
        this.view.setView("Gantt Chart", ganttController.getView());

        // Event handler for logout
        this.view.getSidePane().setOnLogout(() -> {
            user = null;
            studyProfile = null;
            if (onLogout != null) onLogout.run();
        });

        // Listener for when a list item in the side panel is pressed
        this.view.getSidePane().setActiveListener(activeIndex -> {
            if (activeIndex == this.view.getSidePane().indexOf("Add Task")) {
                this.createTaskView();
            }
            this.view.setActive(activeIndex);
        });

        this.view.getSidePane().getOpenFile().setOnAction(event -> importProfileFromFile(owner));

        // If the user does not have a semester study profile associated with it, prompt the user to import the hub file.
        if (studyProfile == null) {
            try {
                importController = DashboardImportController.fromFXML();
                importController.setOnChooseFile(() -> importProfileFromFile(owner));
                this.view.setContentView(importController.getView());
                this.view.getSidePane().setEnabled(false);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * Opens a File chooser dialog to select a semester study profile hub file.
     * @param owner
     */
    private void importProfileFromFile(Window owner) {
        // Open file to import
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Import Semester Study Profile");
        File file = chooser.showOpenDialog(owner);
        // If file is null, user cancelled so do nothing
        if (file == null) return;
        studyProfile = SemesterStudyProfile.importHubFile(file);
        this.view.getSidePane().setEnabled(true);
        this.view.setActive(0);

        updateControllers();
    }

    /**
     * Sets the controllers user model
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        this.getView().getSidePane().getUserPane().setName(user.getFullName());
        this.getView().getSidePane().getUserPane().setEmail(user.getEmail());
    }

    /**
     * Constructs the create task controller and view
     */
    public void createTaskView() {
        CreateTask createTaskView = new CreateTask();
        createTaskController = new CreateTaskController(createTaskView, studyProfile, this);
        createTaskController.setOnTaskAdded(() -> ganttController.refreshModel());
        this.view.setView("Add Task", createTaskView);
    }

    /**
     * Updates controllers. Call this method when the model has been updated and the view needs to refresh.
     */
    public void updateControllers(){
        ganttController.setProfile(studyProfile);
        deadlinesController.setProfile(studyProfile);

        createTaskView();

        moduleController = new ModuleController(this.view, studyProfile.getModules());
        this.view.setView("Modules", moduleController.getModulePaneView());
        
        moduleController.setAssessmentClickHandler(assessmentName -> {
            this.view.setContentView(new AssessmentView(this, assessmentName));
        });

        //For each module, update the modulePane view.
        for (int i = 0; i < studyProfile.getModules().size(); i++) {
            System.out.println(studyProfile.getModules().get(i));
            moduleController.updateView(i);
        }
        user.setSemesterStudyProfile(studyProfile);

        this.view.setView("Modules", moduleController.getModulePaneView());
    }

    /**
     * Sets the study profile model.
     * @param studyProfile
     */
    public void setStudyProfile(SemesterStudyProfile studyProfile) {
        this.studyProfile = studyProfile;
        this.getView().setActive(0);
        this.getView().getSidePane().setEnabled(true);
        updateControllers();

    }

    public boolean hasStudyProfile() {
        return studyProfile != null;
    }

    public SemesterStudyProfile getProfile() {
        return studyProfile;
    }

    public DashboardPane getView() {
        return view;
    }

    public void setModuleInfoView(String code) {
        moduleController.secondViewInfo(code);
    }

    public void createTaskFromAssessment(String module, String assessment) {
        createTaskController.setAssessmentPrefilled(module, assessment);
        this.getView().setContentView(createTaskController.getView());
    }

    public void saveProfile() {
        File file = new File("UserFiles/" + user.getEmail() + ".ser");
        SemesterStudyProfile.saveToFile(file, studyProfile);
    }

    public void setOnLogout(Runnable onLogout) {
        this.onLogout = onLogout;
    }

}
