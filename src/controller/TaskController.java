/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.ActivityController.activToActivityInfo;
import static controller.ActivityController.taskToTaskInfo;
import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import model.*;
import model.StudyActivity.Type;
import view.AlertBox;
import view.ConfirmBox;
import view.assessment.TaskInfo;
import view.assessment.TaskInfo.*;
import view.assessment.TaskView;

/**
 * Task Controller
 * 
 * Controller for Task view. Gets the information of a specific task
 * from the SemesterStudyProfile of the user and sends the information to the view
 * 
 */
public class TaskController {
    private SemesterStudyProfile semester;
    private File file;
    private StudyTask task;
    private final DashboardController dashboard;
    
    private final TaskView view;
    
    public TaskController(DashboardController dashboard,TaskView view, String assessmentName, String taskName){
        this.view = view;
        //file = new File("semesterProfile.ser");     //TEST FILE
        //Loading semester object
        this.dashboard = dashboard;
        semester = dashboard.getProfile();
        // Sending information to the view
        search:
        for(Assessment a : semester.getAssessments()){
            if(a.getName().equals(assessmentName)){
                for(StudyTask t : a.getTasks()){
                    if(t.getName().equals(taskName)){
                        task = t;
                        TaskInfo task = taskToTaskInfo(t);
                        for(StudyTask rt : t.getRequiredTasks()){
                            task.addRequiredTask(taskToTaskInfo(rt));
                        }
                        for(Note n : t.getNotes()){
                            task.addNote(new TaskInfo.NoteInfo(n.getNotes(), n.getDateStr()));
                        }
                        for(StudyActivity act : t.getActivities()){
                            task.addActivity(activToActivityInfo(act));
                        }
                        view.setTask(task);
                        break search;
                    }
                }
            }
        }
    }

    public boolean setName(String newName) {
        boolean success = true;
        // Can't set new name if it is empty
        if(newName==null || newName.isEmpty()){
            success = false;
            AlertBox.display("Invalid entry","You must write a name for the task.");
        }
        // Can't set new name if there is another task with that name
        if(!newName.equals(task.getName())){
            for(StudyTask t : task.getAssessment().getTasks()){
                if(t.getName().equals(newName)){
                    success = false;
                    AlertBox.display("Invalid entry", "There's already a "
                         + "task with that name. Try a different name.");
                    break;
                }
            }
            if(success){
                task.setName(newName);
                dashboard.saveProfile();
                dashboard.updateControllers();
            }
        } 
        return success;
    }
    
    public void setDescription(String newDesc){
        task.setDescription(newDesc);
        dashboard.saveProfile();
        dashboard.updateControllers();
    }
    
    public boolean setTimeToSpend(int newDuration) {
        boolean success = true;
        // Can't set negative numbers or 0
        if(newDuration<=0){
            success = false;
            AlertBox.display("Invalid entry", "You must enter a number superior "
                    + "to zero.");
        }
        else{
            Date deadline = task.getAssessment().getDeadline();
            Date start = task.getAssessment().getStartDate();
            long diffTime = deadline.getTime() - start.getTime();
            long diffDays = diffTime / (1000 * 60 * 60 * 24);
            // A task should not take longer than the whole assessment from start to end date
            if(newDuration>diffDays){
                success = false;
                AlertBox.display("Invalid entry", "The number of days must not be"
                        + " superior the number of days between the start date and"
                        + " the deadline (" + diffDays + ").");
            }
        }
        if(success){
            task.setTimeToBeSpend(Duration.ofDays((long)newDuration));
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        return success;
    }
    
    public TaskInfo addRequiredTask(String taskName) {
        for(StudyTask t : task.getAssessment().getTasks()){
            if(t.getName().equals(taskName)){
                task.addDependency(t);
                dashboard.saveProfile();
                dashboard.updateControllers();
                return taskToTaskInfo(t);
            }
        }
        AlertBox.display("Error!", "Something went wrong. Task was not added.");
        return null;
    }
    public boolean removeRequiredTask(String taskName) {
        boolean remove = false;
        // Prompts user to confirm they want to remove a required task
        if(ConfirmBox.display("Remove note","Are you sure you want to remove task \"" 
                + taskName + "\" as a required task for this task?")){
            for(StudyTask t : task.getRequiredTasks()){
                if(t.getName().equals(taskName)){
                    remove = task.removeRequiredTask(t);
                    dashboard.saveProfile();
                    dashboard.updateControllers();
                    return remove;
                }
            }
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
       return remove;
    }
    public boolean addActivity(ActivityInfo activ){
        boolean added = false;
        Type type = null;
        for(Type t : Type.values()){
            if(t.toString().equals(activ.getType())){
                type = t;
            }
        }
        if(type!=null){
            StudyActivity sAct = new StudyActivity(activ.getName(),task,activ.getTarget(),type);
            task.addActivity(sAct);
            dashboard.saveProfile();
            dashboard.updateControllers();
            added = true;
        }
        
        return added;
    }
    
    public NoteInfo addNote(String noteTxt) {
        for(Note n : task.getNotes()){
            // Can't add a note if there is one with the exact same content
            if(n.getNotes().equals(noteTxt)){
                AlertBox.display("Invalid entry", "You've already entered a note with that same content.");
                return null;
            }
        }
        Note note = new Note(noteTxt, new Date());
        NoteInfo noteInfo = new NoteInfo(note.getNotes(), note.getDateStr());
        task.addNote(note);
        dashboard.saveProfile();
        dashboard.updateControllers();
        return noteInfo;
    }
    
    public boolean removeNote(String noteTxt) {
        // Prompts user to confirm they want to remove a note
        boolean remove = ConfirmBox.display("Remove note","Are you sure you want to delete this note?");
        if(remove){
            remove = task.removeNote(noteTxt);
            dashboard.saveProfile();
            dashboard.updateControllers();
        }
        return remove;
    }
    
    public ArrayList<String> getTaskOptions(){
        ArrayList<String> taskOptions = new ArrayList<>();
        for(StudyTask t : task.getAssessment().getTasks()){
            if(!task.getRequiredTasks().contains(t) && t!=task){
                taskOptions.add(t.getName());
            }
        }
        return taskOptions;
    }

    public void deleteTask() {
        task.getAssessment().getTasks().remove(task);
        dashboard.saveProfile();
        dashboard.updateControllers();
    }
}
