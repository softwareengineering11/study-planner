package controller;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import view.LoginForm;
import view.SceneManager;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LoginController {
    private User user;
    private LoginForm view;
    private EventHandler<ActionEvent> registerHandler;
    private String email;
    private String pw;

    private EventHandler<ActionEvent> loginSuccessHandler;

    public LoginController() {
        view = new LoginForm();
        view.setRegisterHandler(event -> {
            if(registerHandler != null) registerHandler.handle(event);
        });

        view.setLoginHandler(event -> { // If User clicks login..
            email = view.getEmail();
            pw =view.getPw();

            if(login(email,pw)){ // Try to login

                JSONObject userDetails = fromJSON(email);
                String password = userDetails.get("Password").toString();
                String email = userDetails.get("Email").toString();
                String fullName = userDetails.get("Full Name").toString();

                user = new User(email,password,fullName);

                if(loginSuccessHandler!=null )loginSuccessHandler.handle(event);
                view.setEmpty();
                //System.out.println("login");
                //sceneManager.changeScene("Dashboard"); //Change to dashboard
            }
            else{
                view.unableToLogin();
            }


        });

    }

    public User getUser() {
        return user;
    }

    public LoginForm getView() {
        return view;
    }

    public void setRegisterHandler(EventHandler<ActionEvent> registerHandler) {
        this.registerHandler = registerHandler;
    }

    public void setLoginSuccessHandler(EventHandler<ActionEvent> loginSuccessHandler) {
        this.loginSuccessHandler = loginSuccessHandler;
    }

    public JSONObject fromJSON(String email){
        String fileName = "UserFiles/"+email+".json";
        JSONParser parser = new JSONParser();
        if(new File(fileName).exists()){
            try{
                JSONObject userDetails = (JSONObject) parser.parse(new FileReader("UserFiles/"+email+".json"));
                return userDetails;
            }catch(Exception e){

                //e.printStackTrace();
            }

        }
        System.out.println("File not found");
        return null;
    }

    public boolean login(String email,String pw){
        JSONObject userDetails = fromJSON(email);

        if(userDetails==null)return false;
        String password = userDetails.get("Password").toString();

        if (pw.equals(password)){return true;}
        return false;



    }


}
