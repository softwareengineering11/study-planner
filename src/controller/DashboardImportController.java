package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import view.SceneManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DashboardImportController implements Initializable {

    private Node view;

    private Runnable onChooseFile;

    /**
     * Creates the controller and view from FXML.
     * @return
     * @throws IOException
     */
    public static DashboardImportController fromFXML() throws IOException {
        FXMLLoader loader = new FXMLLoader(SceneManager.class.getResource("fxml/DashboardImportTemplate.fxml"));
        Node view = loader.load();
        DashboardImportController controller = loader.getController();
        view.getStyleClass().add("dashboard-import-view");
        controller.view = view;
        return controller;
    }

    public DashboardImportController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void importFile(ActionEvent actionEvent) {
        if (onChooseFile != null) onChooseFile.run();
    }

    public void setOnChooseFile(Runnable onChooseFile) {
        this.onChooseFile = onChooseFile;
    }

    public Node getView() {
        return view;
    }

}
