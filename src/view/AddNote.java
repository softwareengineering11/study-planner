package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Add Note pop up window
 * 
 */
public class AddNote {
    
    static String noteTxt = null;
    
    public static String display(String activTaskName, int activOrTask){
        //activOrTask = 0 -> activity
        //activOrTask = 1 -> task
        
        Stage window = new Stage();
        
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add new note");
        window.setMinWidth(250);
        
        Label title = new Label("Add new note");
        title.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
        Label label;
        if(activOrTask==0){
            label = new Label("Activity: " + activTaskName);
        }
        else if(activOrTask==1){
            label = new Label("Task: " + activTaskName);
        }
        else{
            label = new Label(activTaskName);
        }
        
        TextArea text = new TextArea();
        text.setPrefWidth(500);
        text.setPrefHeight(200);
        text.setWrapText(true);
        
        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        
        saveButton.setOnAction(e -> {
            noteTxt = text.getText();
            window.close();
        });
        
        cancelButton.setOnAction(e -> {
            noteTxt = null;
            window.close();
        });
        
        HBox buttons = new HBox();
        buttons.getChildren().addAll(saveButton, cancelButton);
        buttons.setSpacing(15);
        buttons.setAlignment(Pos.CENTER_LEFT);
        
        VBox layout = new VBox();
        layout.setPadding(new Insets(15,20,15,20));
        layout.setSpacing(15);
        layout.getChildren().addAll(title,label, text,buttons);
        layout.setAlignment(Pos.CENTER_LEFT);
        
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        
        return noteTxt;
    }
}
