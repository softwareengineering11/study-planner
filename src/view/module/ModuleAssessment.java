package view.module;

import java.util.Date;

//Class will store information about an assessment.
public class ModuleAssessment {
    private String name;
    private String startDate;
    private String deadline;
    private int weight;
    
    
    
    //Constructor
    public ModuleAssessment(String name, String startDate, String deadline, int weight){
        this.name = name;
        this.startDate = startDate;
        this.deadline = deadline;
        this.weight = weight;
    }

    //Accessor Methods
    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getDeadline() {
        return deadline;
    }

    public int getWeight() {
        return weight;
    }
    
}
