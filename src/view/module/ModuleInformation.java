package view.module;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ModuleInformation extends BorderPane {
    private String moduleName;
    private String moduleCode;
    private String description;
    private ArrayList<ModuleAssessment> assessments;
    private EventHandler<ActionEvent> backButtonClickHandler;
    private AssessmentButtonClickHandler assessmentClickHandler;
    
    //Constructor
    public ModuleInformation(){
        moduleName = "";
        moduleCode = "";
        description = "";
        assessments = new ArrayList<>();
    }
    
    //Method which stores the information
    public void addInformation(String name, String code, String description){
        moduleName = name;
        moduleCode = code;
        this.description = description;
        updateBorderPane();
    }
    
    public void addAssessment(ModuleAssessment assessment){
        assessments.add(assessment);
        updateBorderPane();
    }
    
    public void updateBorderPane(){
        this.getChildren().clear(); //Clears the BorderPane
        
        //Creates a new text for the module name, code and description
        Text assessmentTitle = new Text("Assessments");
        Text nameAndCode = new Text(this.moduleName + " - " + this.moduleCode);
        Text mainInfo = new Text(nameAndCode.getText() + "\n" + this.description 
                    + "\n" + assessmentTitle.getText());
        mainInfo.setTextAlignment(TextAlignment.CENTER);
        mainInfo.setStyle("-fx-font-weight: bold; -fx-font-size: 15pt");
        
        Button backButton = new Button("< Back to Modules"); //Back button
        backButton.setStyle("-fx-text-fill: black");
        backButton.setOnAction((event) -> {
            if(backButtonClickHandler != null) backButtonClickHandler.handle(event);
        });
        
        BorderPane borderPane = new BorderPane();
        this.setTop(borderPane);
        
        borderPane.setLeft(backButton);
        borderPane.setCenter(mainInfo);
        
        ScrollPane scrollPane = new ScrollPane();
        FlowPane flowPane = new FlowPane();
        
        this.setCenter(flowPane);
        flowPane.prefWidthProperty().bind(scrollPane.widthProperty());
        flowPane.prefHeightProperty().bind(scrollPane.heightProperty());
        flowPane.setHgap(40);
        flowPane.setVgap(40);
        flowPane.setAlignment(Pos.CENTER); //Positions the flowpane
        flowPane.setPadding(new Insets(0,0,0,80)); //Sets padding on the right
        flowPane.setStyle("-fx-background-color: TRANSPARENT");
        
        
        for(ModuleAssessment a : assessments){
            int counter = 0;
            Button assessmentDetails = new Button(a.getName() + "\n" + 
                                              "Start Date :\t" + a.getStartDate() + "\n" + 
                                              "Deadline :\t" + a.getDeadline() + "\n" +
                                              "Weight :\t\t" + a.getWeight() + "%");
            assessmentDetails.setTextAlignment(TextAlignment.LEFT);
            assessmentDetails.setPrefSize(250, 180);
            assessmentDetails.setStyle("-fx-background-color: #bababa; -fx-font-size: 16;"
                    + "-fx-font-weight: bold; -fx-text-fill: black;");
            assessmentDetails.setWrapText(true);
            flowPane.getChildren().add(assessmentDetails);
            
            assessmentDetails.setOnMouseEntered(event -> {
                assessmentDetails.setStyle("-fx-background-color: #a8a8a8; -fx-text-fill: black; "
                        + "-fx-font-size: 16; -fx-font-weight: bold;");
                assessmentDetails.setWrapText(true);
            });
            
            assessmentDetails.setOnMouseExited(event -> {
                assessmentDetails.setStyle("-fx-background-color: #bababa; -fx-text-fill: black; "
                        + "-fx-font-size: 16; -fx-font-weight: bold;");
                assessmentDetails.setWrapText(true);
            });
            
            assessmentDetails.setOnMouseClicked(event -> {
                if(assessmentClickHandler != null) assessmentClickHandler.handle(a.getName());
            });
        }
        
        scrollPane.setContent(flowPane);
    }
    
    public void clearBorderPane(){
        assessments.clear();
    }
    
    public void setBackClickHandler(EventHandler buttonClickHandler){
        this.backButtonClickHandler = buttonClickHandler;
    }
    
    public interface AssessmentButtonClickHandler{
        void handle(String assessmentName);
    }
    
    public void setAssessmentClickHandler(AssessmentButtonClickHandler assessmentClickHandler){
        this.assessmentClickHandler = assessmentClickHandler;
    }
}
