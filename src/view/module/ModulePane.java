package view.module;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import java.util.HashMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextAlignment;

public class ModulePane extends ScrollPane {
    
    //Stores the module name and module code as key-value pairs
    private HashMap<String, String> modules;
    private ButtonClickHandler buttonClickHandler;
    
    private FlowPane flowPane;
    
    public ModulePane(){
        modules = new HashMap<>();
        
        flowPane = new FlowPane();
        
        flowPane.prefWidthProperty().bind(this.widthProperty());
        flowPane.prefHeightProperty().bind(this.heightProperty());
        flowPane.setHgap(40); //Sets the gap between buttons (horizontal)
        flowPane.setVgap(40); //Sets the gap between buttons (vertical)
        flowPane.setAlignment(Pos.CENTER); //Positions the flowpane
        flowPane.setStyle("-fx-background-color: TRANSPARENT");
        
        
        this.setContent(flowPane);
    }
    
    /*Method which adds the module name and code into the hashmap 
    and calls the flowpane method*/
    public void addModule(String name, String code){
        modules.put(code, name);
        updateFlowPane();
    }
    
    //Method which updates the flowpane
    public void updateFlowPane(){
        flowPane.getChildren().clear(); //Clears the flowpane
        
        //For each module name in the hashmap, create a new button and add to flowpane
        for(String code : modules.keySet()){
            Button button = new Button(code + "\n" + modules.get(code)); //Creates a new button
            button.setPrefWidth(250); //Sets width of button
            button.setPrefHeight(200); //Sets height of button
            button.setTextAlignment(TextAlignment.CENTER); //Centres the text
            button.setStyle("-fx-background-color: #bababa; -fx-text-fill: black; "
                    + "-fx-font-size: 20; -fx-font-weight: bold;");
            button.setWrapText(true);
            
            button.setOnMouseEntered(event -> {
                button.setStyle("-fx-background-color: #a8a8a8; -fx-text-fill: black; "
                        + "-fx-font-size: 20; -fx-font-weight: bold;");
                button.setWrapText(true);
            });
            
            button.setOnMouseExited(event -> {
                button.setStyle("-fx-background-color: #bababa; -fx-text-fill: black; "
                        + "-fx-font-size: 20; -fx-font-weight: bold;");
                button.setWrapText(true);
            });
            
            button.setOnMouseClicked(event -> {
                if(buttonClickHandler != null) buttonClickHandler.handle(code);
            });
            
            
            flowPane.getChildren().add(button); //Adds button to flowpane
        }
    }
    
    public void setModuleClickHandler(ButtonClickHandler buttonClickHandler){
        this.buttonClickHandler = buttonClickHandler;
    }
    
    public interface ButtonClickHandler{
        void handle(String code);
    }
}
