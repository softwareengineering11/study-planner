package view;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CreateAccountForm extends GridPane{

    private EventHandler<ActionEvent> createHandler;
    private EventHandler<ActionEvent> backHandler;
    private StringProperty email;
    private StringProperty password;
    private StringProperty fullName;
    private StringProperty cantRegister;


    public CreateAccountForm() {
        this.getStyleClass().add("container");
        this.setAlignment(Pos.TOP_CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setMaxHeight(480.0);
        this.setMaxWidth(480.0);

        email= new SimpleStringProperty();
        password= new SimpleStringProperty();
        fullName = new SimpleStringProperty();
        cantRegister = new SimpleStringProperty();

        this.getStylesheets().add(SceneManager.class.getResource("style/CreateAccount.css").toExternalForm());

        Font labelFont = null;
        Font welcomeFont = null;
        try {
            labelFont = Font.loadFont(new FileInputStream("res/ProxNova.ttf"), 24);
            welcomeFont = Font.loadFont(new FileInputStream("res/Font2.ttf"), 36);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (labelFont == null) labelFont = Font.font(24);

        else if (welcomeFont == null) welcomeFont = Font.font(36);
        Font buttonFont = Font.font(labelFont.getFamily(), 16);




        Button back = new Button("<   Back to login");
        back.getStyleClass().add("back");
        back.setFont(buttonFont);
        back.setStyle("-fx-border-color:transparent;");
        this.add(back,0,0,1,1);
        this.setMargin(back, new Insets(10, 0, 0, -70));
        //CA

        Text headerText = new Text("Create Account");
        headerText.setFont(welcomeFont);
        headerText.setFill(Color.WHITE);
        this.add(headerText, 0, 0, 2, 2);
        this.setMargin(headerText, new Insets(60, 0, 0, 15));


        // Full name
        TextField nameBox = new TextField();
        nameBox.setPromptText("Full Name");
        nameBox.setFont(labelFont);
        this.add(nameBox, 0, 3, 2, 2);
        this.setMargin(nameBox, new Insets(25, 0, 0, 0));


        //Email
        TextField emailBox = new TextField();
        emailBox.setPromptText("Email/username");
        emailBox.setFont(labelFont);
        this.add(emailBox, 0, 6, 2, 2);
        //this.setMargin(emailBox, new Insets(-10, 0, 0, 0));

        //Pw Box
        TextField passwordBox = new TextField();
        passwordBox.setPromptText("Password");
        passwordBox.setFont(labelFont);
        this.add(passwordBox, 0, 9, 2, 2);

        // Create account VBOX
        Button createButton = new Button("Create Account");
        createButton.setPrefWidth(150);
        createButton.setPrefHeight(50);
        createButton.setFont(buttonFont);

        Label unable = new Label("");
        unable.setFont(buttonFont);
        unable.setStyle("-fx-text-fill:red;");






        VBox formControls = new VBox(10);
        formControls.setAlignment(Pos.BOTTOM_CENTER);
        formControls.getChildren().add(createButton);
        formControls.getChildren().add(unable);
        this.add(formControls,0,12,3,3);
        this.setMargin(formControls,new Insets(30,0,0,0));

        // UI Events
        createButton.setOnMouseEntered(event -> createButton.setStyle("-fx-text-fill:blue;"));

        createButton.setOnMouseExited(event -> createButton.setStyle("-fx-background-color:transparent;"));

        createButton.setOnAction(event -> {
            if(createHandler != null){
                createHandler.handle(event);
            }
            //unable.setStyle("-fx-text-fill:red;");
        });

        back.setOnMouseEntered(event -> back.setStyle("-fx-text-fill:blue;"));

        back.setOnMouseExited(event -> back.setStyle("-fx-background-color:transparent;"));

        back.setOnAction(event -> {
            if(backHandler!=null){
                unable.setStyle("-fx-text-fill:transparent;");
                backHandler.handle(event);
            }

        });



        email.bindBidirectional(emailBox.textProperty());
        fullName.bindBidirectional(nameBox.textProperty());
        password.bindBidirectional(passwordBox.textProperty());
        cantRegister.bindBidirectional(unable.textProperty());

    }

    public void cantRegister(){
        cantRegister.setValue("Unable to login ");
    }

    public void setEmpty(){
        email.setValue("");
        fullName.setValue("");
        password.setValue("");
    }

    public void setCreateHandler(EventHandler<ActionEvent> createHandler) {
        this.createHandler = createHandler;
    }

    public void setBackHandler(EventHandler<ActionEvent> backHandler) {
        this.backHandler = backHandler;
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public String getFullName() {
        return fullName.get();
    }

    public StringProperty fullNameProperty() {
        return fullName;
    }
}
