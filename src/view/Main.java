package view;

import controller.DashboardController;
import controller.CreateAccountController;
import controller.LoginController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import model.SemesterStudyProfile;
import model.User;
import view.dashboard.DashboardPane;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static model.SemesterStudyProfile.loadFile;

public class Main extends Application {

    public static final String APP_TITLE = "Study Planner";
    private static final double APP_DEFAULT_WIDTH = 1280.0;
    private static final double APP_DEFAULT_HEIGHT = 720.0;

    private SceneManager sceneManager;

    private DashboardPane dashboardView;
    private DashboardController dashboardController;

    private BorderPane loginContainer;
    private CreateAccountController createAccountController;
    private LoginController loginController;

    private Runnable onLogout;

    public static void main(String[] args) {
        launch(args);
    }

    private Pane createRootPane(){
        StackPane root = new StackPane();
        root.getStyleClass().add("login-root");

        // ui pane
        loginContainer = new BorderPane();
        loginContainer.setPrefWidth(300.0);
        //loginContainer.setAlignment(Pos.CENTER);

        createAccountController = new CreateAccountController();
        loginController = new LoginController();

        loginController.setRegisterHandler(event -> loginContainer.setCenter(createAccountController.getView()));
        loginController.setLoginSuccessHandler(event -> {

            User user = loginController.getUser();
            dashboardController.setUser(user);

            SemesterStudyProfile semesterStudyProfile = getUserSemesterStudyProfileFromFile(user);

            user.setSemesterStudyProfile(semesterStudyProfile);
            //System.out.println(user.hasSemesterStudyProfile());
            if (user.hasSemesterStudyProfile()) {

                dashboardController.setStudyProfile(user.getSemesterStudyProfile());
            }
            sceneManager.changeScene("Dashboard");
        });

        createAccountController.setRegisterSuccessHandler(event ->{
            User user = createAccountController.getUser();
            dashboardController.setUser(user);
            sceneManager.changeScene("Dashboard");
        });

        createAccountController.setBackHandler(event -> loginContainer.setCenter(loginController.getView()));

        loginContainer.setCenter(loginController.getView());

        AnchorPane uiPane = new AnchorPane();
        uiPane.getChildren().add(loginContainer);

        AnchorPane.setLeftAnchor(loginContainer, 0.0);
        AnchorPane.setRightAnchor(loginContainer, 0.0);
        AnchorPane.setBottomAnchor(loginContainer, 0.0);
        AnchorPane.setTopAnchor(loginContainer, 0.0);

        root.getChildren().addAll(uiPane);

        return root;
    }

    private SemesterStudyProfile getUserSemesterStudyProfileFromFile(User user){
        SemesterStudyProfile semesterStudyProfile = null;
        if(new File("UserFiles/"+user.getEmail()+".ser").exists()) {

            File file = new File("UserFiles/" + user.getEmail() + ".ser");
            try {
                semesterStudyProfile = loadFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }
        return semesterStudyProfile;
    }

    private Scene createScene(Parent root){
        return new Scene(root, APP_DEFAULT_WIDTH, APP_DEFAULT_HEIGHT);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle(APP_TITLE);

        sceneManager = new SceneManager();
        dashboardView = new DashboardPane();
        dashboardController = new DashboardController(primaryStage, dashboardView);

        onLogout = ()-> {
            loginContainer.setCenter(loginController.getView());
            sceneManager.changeScene("Login");
            dashboardController = new DashboardController(primaryStage, dashboardView);
            dashboardController.setOnLogout(onLogout);
        };

        dashboardController.setOnLogout(onLogout);

        sceneManager.add("Login", createScene(createRootPane()));
        sceneManager.add("Dashboard", createScene(dashboardView));
        sceneManager.setOnSceneChange((scene -> primaryStage.setScene(scene)));

        sceneManager.changeScene("Login");

        primaryStage.show();
    }
}
