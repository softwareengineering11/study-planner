package view.deadlines;

/**
 * Helper class to store information of a deadline (assessment) needed for
 * displaying in the DeadlinesPane
 */
public class DeadlineInfo {
    private String name;
    private String date;
    private float progress;

    public DeadlineInfo(String name, String date, float progress) {
        this.name = name;
        this.date = date;
        this.progress = progress;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public float getProgress() {
        return progress;
    }
    
    
}
