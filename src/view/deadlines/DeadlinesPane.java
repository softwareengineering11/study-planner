package view.deadlines;

import controller.DashboardController;
import controller.DeadlinesController;
import java.util.ArrayList;
import java.util.Collections;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import view.CustomSeparator;
import view.assessment.AssessmentView;

/**
 * Deadlines View
 * 
 * Displays deadlines in four different categories, in a grid:
 *      upcoming deadlines        deadlines in progress
 *      missed deadlines          completed deadlines
 * 
 */
public class DeadlinesPane extends GridPane {
    private VBox upcoming, missed, completed, inProgress;
    private ArrayList<DeadlineInfo> upcomingDL, missedDL, completedDL, inProgressDL;
    private DoubleBinding panelWidth, panelHeight, width, height;
    
    private DeadlineClickHandler deadlineClickHandler;
    
    public DeadlinesPane(DashboardController parent) {
        getStyleClass().add("deadlines");
        
        /* Setting the click handler of a deadline to take to the assessment view
         * of the respective assessment */
        deadlineClickHandler = (assessmentName -> {
            parent.getView().setContentView(new AssessmentView(parent, assessmentName));
        });
        
        
        panelWidth = parent.getView().widthProperty().multiply(0.80).subtract(60);
        panelHeight = parent.getView().heightProperty().subtract(90);
        width = panelWidth.multiply(0.5);
        height = panelHeight.multiply(0.5);
        
        prefWidthProperty().bind(width);
        setHgap(20);
        setVgap(20);
        setAlignment(Pos.CENTER);
        
        upcomingDL = new ArrayList<>();
        missedDL= new ArrayList<>();
        completedDL= new ArrayList<>();
        inProgressDL = new ArrayList<>();
    }
    
    /* Creates view by creating each of grip units corresponding to each category */
    public void createView(){

        // 1 - Left top (Upcoming)
        upcoming = createGridUnit("Upcoming Deadlines", upcomingDL);
        this.add(upcoming, 0,0);

        // 2 - Right top (inProgress)
        inProgress = createGridUnit("Deadlines in Progress", inProgressDL);
        this.add(inProgress, 1,0);

        // 3 - Left Bottom (Missed)
        missed = createGridUnit("Missed Deadlines", missedDL);
        this.add(missed, 0,1);

        // 4 - Right Bottom (Completed)
        Collections.reverse(completedDL);  //Reserve to show most recent ones first 
        completed = createGridUnit("Completed Deadlines", completedDL);
        this.add(completed, 1,1);
    }
    
    /* Creates a grid unit by parsing as arguments  the title of the category, 
     * and the list of the deadlines in that category */
    public VBox createGridUnit(String titleStr, ArrayList<DeadlineInfo> deadlines){
        VBox gridUnit = new VBox();
        gridUnit.getStyleClass().add("deadlines-grid-unit");
        gridUnit.prefWidthProperty().bind(width);
        gridUnit.prefHeightProperty().bind(height);
        //title
        Label title = new Label(titleStr);
        title.getStyleClass().add("deadlines-title");
        title.prefWidthProperty().bind(width);
        //separator
        CustomSeparator sep = new CustomSeparator();
        sep.getStyleClass().add("deadlines-separator");
        gridUnit.getChildren().addAll(title, sep);
        //entry list
        int dlCount = deadlines.size();
        VBox allEntries = new VBox();
        allEntries.prefWidthProperty().bind(width);
        for(int i=0; i<dlCount; i++){
            VBox entryBox = new VBox();
            //Deadline name = first line of each entry
            entryBox.getStyleClass().add("deadlines-entry");
            Label dlName = new Label(deadlines.get(i).getName());
            dlName.getStyleClass().add("deadlines-entry-name");
            dlName.setPadding(new Insets(5, 15, 5, 15));
            // Date + Progress bar = second line of each entry
            Label dlDate = new Label(deadlines.get(i).getDate());
            dlDate.getStyleClass().add("deadlines-entry-date");
            HBox dateAndBar = new HBox(50);
            dateAndBar.setPadding(new Insets(0, 15, 5, 35));
            dateAndBar.getChildren().addAll(dlDate,
                            createProgressBar(deadlines.get(i).getProgress()));
            entryBox.getChildren().addAll(dlName,dateAndBar);
            //Setting on mouse click event
            final String assessmentName = deadlines.get(i).getName();
            entryBox.setOnMouseClicked(event -> {
                if(deadlineClickHandler != null)
                    deadlineClickHandler.handle(assessmentName);
            });
            allEntries.getChildren().add(entryBox);
        }
        // Entries are contained within a scroll pane so it can be scrollable
        // (in case of many entries in one category)
        ScrollPane sp = new ScrollPane(allEntries);
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        gridUnit.getChildren().add(sp);
        sp.setStyle("-fx-background:transparent;-fx-background-color:transparent;");
        return gridUnit;
    }
    
    // Creates a progress bar by parsing as argument a float
    public static StackPane createProgressBar(float progress){
        //Forcing progress to be a value between 0 and 1
        if(progress<0) progress=0;
        else if(progress>1) progress = 1;
        
        float barTotalWidth = 100.0f;
        StackPane progressBar = new StackPane();
        //background of bar
        Rectangle bgBar = new Rectangle(barTotalWidth,15.0);
        bgBar.setFill(Color.web("#a6a6a6"));
        //progress bar
        float barWidth = barTotalWidth*progress;
        Rectangle bar = new Rectangle(barWidth,15.0);
        bar.setFill(Color.rgb(24, 32, 71));

        progressBar.getChildren().addAll(bgBar, bar);
        progressBar.setAlignment(Pos.CENTER_LEFT);
        return progressBar;
    }

    public void clearView() {
        this.getChildren().clear();
        this.upcomingDL.clear();
        this.completedDL.clear();
        this.inProgressDL.clear();
        this.missedDL.clear();
    }

    public interface DeadlineClickHandler{
        void handle(String assessmentName);
    }
    
    public void addToCompleted(DeadlineInfo deadline){
        completedDL.add(deadline);
    }
    
    public void addToUpcoming(DeadlineInfo deadline){
        upcomingDL.add(deadline);
    }
    
    public void addToMissed(DeadlineInfo deadline){
        missedDL.add(deadline);
    }
    
    public void addToInProgress(DeadlineInfo deadline){
        inProgressDL.add(deadline);
    }
    
    public int getUpcomingSize(){
        return upcomingDL.size();
    }
}
