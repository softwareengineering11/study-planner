package view.dashboard;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import view.CustomSeparator;

public class DashboardUserPane extends BorderPane {

    private static final double DISPLAY_PIC_SIZE = 70;

    private StringProperty nameProperty;
    private StringProperty emailProperty;

    public DashboardUserPane(DashboardSidePane parent) {
        getStyleClass().add("dashboard-user-pane");

        nameProperty = new SimpleStringProperty();
        emailProperty = new SimpleStringProperty();

        ImageView displayPic = new ImageView();
        displayPic.setImage(new Image("display.png"));
        displayPic.setFitWidth(DISPLAY_PIC_SIZE);
        displayPic.setFitHeight(DISPLAY_PIC_SIZE);
        displayPic.setClip(new Circle(DISPLAY_PIC_SIZE * 0.5, DISPLAY_PIC_SIZE * 0.5, DISPLAY_PIC_SIZE * 0.5));

        VBox displayPicBox = new VBox();
        displayPicBox.getStyleClass().add("display-pic-box");
        displayPicBox.setAlignment(Pos.CENTER);
        displayPicBox.getChildren().addAll(displayPic);

        VBox userInfoBox = new VBox();
        userInfoBox.setAlignment(Pos.CENTER_LEFT);
        userInfoBox.getStyleClass().add("user-info-box");
        Label nameLabel = new Label("Jason Antonino");
        nameLabel.textProperty().bind(nameProperty);
        Label emailLabel = new Label("jason@istoo.good");
        emailLabel.textProperty().bind(emailProperty);
        userInfoBox.getChildren().addAll(nameLabel, emailLabel);

        prefHeightProperty().bind(parent.heightProperty().multiply(0.2));
        setLeft(displayPicBox);
        setCenter(userInfoBox);
        setBottom(new CustomSeparator());

        BorderPane.setAlignment(displayPicBox, Pos.CENTER_RIGHT);
        BorderPane.setAlignment(userInfoBox, Pos.CENTER_LEFT);
    }

    public String getName() {
        return nameProperty.get();
    }

    public StringProperty nameProperty() {
        return nameProperty;
    }

    public void setName(String nameProperty) {
        this.nameProperty.set(nameProperty);
    }

    public String getEmail() {
        return emailProperty.get();
    }

    public StringProperty emailProperty() {
        return emailProperty;
    }

    public void setEmail(String emailProperty) {
        this.emailProperty.set(emailProperty);
    }
}
