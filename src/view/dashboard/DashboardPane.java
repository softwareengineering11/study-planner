package view.dashboard;




import controller.CreateTaskController;
import controller.ModuleController;
import controller.GanttChartController;

import javafx.application.Platform;
import controller.DashboardImportController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Popup;
import javafx.stage.Window;
import view.SceneManager;
import view.ganttchart.GanttChart;

import java.util.ArrayList;

import view.CreateTask;


import java.util.HashMap;

import view.module.ModuleInformation;
import view.module.ModulePane;
import view.assessment.AssessmentView;
import view.assessment.TaskView;
import view.deadlines.DeadlinesPane;



public class DashboardPane extends BorderPane {

    private HBox headerBox;
    private StringProperty titleProperty;

    private DashboardSidePane sidePane;

    private BorderPane headerContentPane;

    private String[] items = {
            "Deadlines",
            "Modules",
            "Gantt Chart",
            "Add Task",
            "Logout",
    };
    private String[] icons = {
            "icon_deadlines.png",
            "icon_modules.png",
            "icon_gantt.png",
            "icon_add.png",
            "icon_logout.png"
    };


    private HashMap<String, Node> views;
    private HashMap<String, String> iconMap;


    public DashboardPane() {
        getStyleClass().add("dashboard-pane");
        getStylesheets().add(SceneManager.class.getResource("style/dashboard.css").toExternalForm());

        views = new HashMap<>();
        iconMap = new HashMap<>();

        for(int i = 0; i < items.length; ++i){
            views.put(items[i], null);
            iconMap.put(items[i], icons[i]);
        }
        
        // Side Panel
        sidePane = new DashboardSidePane(this, items, icons);
        setLeft(sidePane);

        headerContentPane = new BorderPane();
        
        // Header
        headerBox = new HBox();
        headerBox.getStyleClass().add("header-box");
        populateHeader();
        headerContentPane.setTop(headerBox);


        // Body
        setCenter(headerContentPane);

        
        
    }

    /**
     * Assigns a view node to a string key.
     * @param key
     * @param view
     */
    public void setView(String key, Node view){
        views.put(key, view);
    }

    /**
     * Sets the active view index.
     * Changing this will set the content view to the index-th view.
     * @param index
     */
    public void setActive(int index) {
        headerContentPane.setCenter(views.get(items[index]));
        titleProperty.set(items[index]);
    }

    /**
     * Sets the active view to the view assigned to the provided key.
     * The key must have a view assigned by setView(String key, Node view)
     * @param key
     */
    public void setActive(String key){
        int index = 0;
        for(int i = 0; i < items.length; ++i) if(items[i].equals(key)) index = i;
        setActive(index);
    }

    /**
     * Constructs the header node
     */
    private void populateHeader() {
        titleProperty = new SimpleStringProperty("Dashboard");

        Label title = new Label();
        title.getStyleClass().add("title");
        title.textProperty().bind(titleProperty);

        headerBox.getChildren().add(title);
    }

    public DashboardSidePane getSidePane() {
        return sidePane;
    }

    /**
     * Sets the content view node. The view node does not have to be assigned to a key.
     * @param view
     */
    public void setContentView(Node view) {
        headerContentPane.setCenter(view);
    }
}
