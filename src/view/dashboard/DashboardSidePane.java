package view.dashboard;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import view.CustomSeparator;

public class DashboardSidePane extends BorderPane {

    private DashboardUserPane userPane;

    private VBox listPane;

    private BorderPane controlPane;

    private int activeListEntry;

    private ActiveChangeListener activeListener;

    private Runnable onLogout;

    private Button openFileBtn;
    private boolean enabled;

    private String[] items;

    public DashboardSidePane(DashboardPane parent, String[] items, String[] icons) {
        this.items = items;
        getStyleClass().add("dashboard-side-pane");

        prefWidthProperty().bind(parent.widthProperty().multiply(0.2));

        userPane = new DashboardUserPane(this);

        listPane = new VBox();
        for(int i = 0; i < Math.min(items.length, icons.length); ++i){
            listPane.getChildren().add(createListEntry(items[i], icons[i]));
        }

        controlPane = new BorderPane();
        controlPane.setTop(new CustomSeparator());
        controlPane.setCenter(createControlContainer());

        setTop(userPane);
        setCenter(listPane);
        setBottom(controlPane);

        setActive(0);
    }

    /**
     * Constructs a list item node from the given text and icon.
     * @param text
     * @param iconPath
     * @return Node
     */
    private Node createListEntry(String text, String iconPath) {
        HBox box = new HBox();
        box.getStyleClass().add("dashboard-side-item");
        box.setAlignment(Pos.CENTER_LEFT);

        ImageView icon = new ImageView();
        icon.getStyleClass().add("dashboard-entry-icon");
        icon.setImage(new Image(iconPath));
        icon.setPreserveRatio(true);
        icon.setFitHeight(24.0);

        Label title = new Label(text);
        title.setTextFill(Color.WHITESMOKE);

        box.getChildren().addAll(icon, title);

        box.setOnMouseClicked(event -> {
            if(text.equals("Logout") && onLogout != null) onLogout.run();
            if(!enabled) return;
            int activeIndex = listPane.getChildren().indexOf(box);
            setActive(activeIndex);
            if (activeListener != null) activeListener.onChange(activeIndex);
        });

        return box;
    }

    /**
     * Constructs bottom section of the side pane.
     * @return Node
     */
    private Node createControlContainer(){

        HBox controlContainer = new HBox();
        controlContainer.getStyleClass().add("dashboard-control-container");
        controlContainer.setAlignment(Pos.CENTER_RIGHT);

//        ImageView settingsIcon = new ImageView();
//        settingsIcon.setImage(new Image("icon_settings.png"));
//        settingsIcon.setFitWidth(24);
//        settingsIcon.setFitHeight(24);
//        Button settingsBtn = new Button();
//        settingsBtn.setGraphic(settingsIcon);

        ImageView openFileIcon = new ImageView();
        openFileIcon.setImage(new Image("icon_open.png"));
        openFileIcon.setFitWidth(24);
        openFileIcon.setFitHeight(24);
        openFileBtn = new Button();
        openFileBtn.setTooltip(new Tooltip("Import semester study profile"));
        openFileBtn.setGraphic(openFileIcon);

        controlContainer.getChildren().addAll(openFileBtn);

        return controlContainer;
    }

    /**
     * Returns the open file button node.
     * @return
     */
    public Button getOpenFile() {
        return openFileBtn;
    }

    /**
     * Resets the 'active' state of the side panel items.
     */
    public void resetActive() {
        listPane.getChildren().forEach(node -> node.getStyleClass().remove("active"));
    }

    /**
     * Sets the active list item index.
     * @param active
     */
    public void setActive(int active) {
        resetActive();
        this.activeListEntry = active;
        listPane.getChildren().get(activeListEntry).getStyleClass().add("active");
    }

    public void setActiveListener(ActiveChangeListener listener) {
        this.activeListener = listener;
    }

    /**
     * Enables or disables the side panel list. When disabled, the action listener for the side panel does not fire.
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setOnLogout(Runnable onLogout) {
        this.onLogout = onLogout;
    }

    public int indexOf(String item) {
        for(int i = 0; i < items.length; ++i){
            if(items[i].equals(item)){
                return i;
            }
        }
        return -1;
    }

    public interface ActiveChangeListener {
        void onChange(int activeIndex);
    }

    public DashboardUserPane getUserPane() {
        return userPane;
    }
}
