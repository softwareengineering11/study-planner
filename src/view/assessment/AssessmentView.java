package view.assessment;

import controller.AssessmentController;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import controller.DashboardController;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import view.deadlines.DeadlinesPane;

/**
 * Assessment View
 * 
 * Displays the information of a specific assessment, including a list of all
 * its tasks
 * 
 */
public class AssessmentView extends ScrollPane{
    private String name, type, module, moduleCode, startDate, deadline, description;
    private int weight;
    private float progress;
    private ArrayList<TaskInfo> tasks;
    
    private VBox panel;
    private DoubleBinding width;
    private DashboardController dashboard;
    private AssessmentController controller;
    
    private final ModuleClickHandler moduleClickHandler;
    private final TaskClickHandler taskClickHandler;
    private final AddTaskClickHandler addTaskClickHandler;
    private final EditClickHandler editNameClickHandler, editDescClickHandler,
            editStartClickHandler, editDeadlineClickHandler;
    private final CancelClickHandler cancelNameClickHandler, cancelDescClickHandler,
            cancelStartClickHandler, cancelDeadlineClickHandler;
    private final SaveTxtFieldClickHandler saveNameClickHandler;
    private final SaveTxtAreaClickHandler saveDescClickHandler;
    private final SaveDateClickHandler saveStartClickHandler, saveDeadlineClickHandler;
    
    private Label title, typeLabel, descriptionLbl, startLbl, deadlineLbl;
    private HBox moduleInfo, startDateInfo, deadlineInfo, weightInfo, descriptionInfo,
            progressInfo, tasksLabelBar, addTask;
    private VBox tasksEntries;
    
    public AssessmentView(DashboardController dashboard, String assessmentName) {
        getStyleClass().add("assessment");
        this.dashboard = dashboard;
        tasks = new ArrayList<>();
        controller = new AssessmentController(dashboard,this,assessmentName);
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        /****************** CHANGE VIEW CLICK HANDLERS ***********************/
        moduleClickHandler = (moduleCode_ -> {
            dashboard.setModuleInfoView(moduleCode_);
        });
        taskClickHandler = (taskName -> {
            dashboard.getView().setContentView(new TaskView(dashboard,name,taskName));
        });
        addTaskClickHandler = (() -> {
            dashboard.createTaskFromAssessment(module,name);
        });
        
        /******************** SAVE BUTTONS HANDLERS ***************************/
        /* Each save button must call the controller to try and set the respective
         * field to the value the user entered, and if successful, it must replace
         * input/edit box for a label with the new value */
        saveNameClickHandler = (field -> {
            if(controller.setName(field.getText())){
                name = field.getText();
                title.setText(name);
                panel.getChildren().remove(field.getParent());
                panel.getChildren().add(0, title);
            }
        });
        saveDescClickHandler = (area -> {
            controller.setDescription(area.getText());
            description = area.getText();
            descriptionLbl.setText(description);
            descriptionInfo.getChildren().remove(area.getParent());
            descriptionInfo.getChildren().add(1, descriptionLbl);
        });
        
        saveStartClickHandler = (datePicker -> {
            if(controller.setStartDate(datePicker.getValue())){
                startDate = datePicker.getValue().format(formatter);
                startLbl.setText(startDate);
                startDateInfo.getChildren().remove(datePicker.getParent());
                startDateInfo.getChildren().add(1, startLbl);
            }
        });
        saveDeadlineClickHandler = (datePicker -> {
            if(controller.setDeadline(datePicker.getValue())){
                deadline = datePicker.getValue().format(formatter);
                deadlineLbl.setText(deadline);
                deadlineInfo.getChildren().remove(datePicker.getParent());
                deadlineInfo.getChildren().add(1, deadlineLbl);
            }
        });
        
        /******************** CANCEL BUTTONS HANDLERS *************************/
        /* Cancel buttons simply replace the input/edit box with the label with
         * the old value */
        cancelNameClickHandler = ((editBox) -> {
            panel.getChildren().remove(editBox);
            panel.getChildren().add(0, title);
        });
        cancelDescClickHandler = ((editBox) -> {
            descriptionInfo.getChildren().remove(editBox);
            descriptionInfo.getChildren().add(1, descriptionLbl);
        });
        cancelStartClickHandler = ((editBox) -> {
            startDateInfo.getChildren().remove(editBox);
            startDateInfo.getChildren().add(1, startLbl);
        });
        cancelDeadlineClickHandler = ((editBox) -> {
            deadlineInfo.getChildren().remove(editBox);
            deadlineInfo.getChildren().add(1, deadlineLbl);
        });
        
        /****************** EDIT INFORMATION HANDLERS**************************/
        /* Edit click handler must replace the label of the field clicked
         * (if editable) with an appropriate input/edit box, and save and cancel buttons*/
        editNameClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced);
            TextField text = (TextField)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveNameClickHandler != null)
                    saveNameClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelNameClickHandler != null)
                    cancelNameClickHandler.handle(editBox);
            });
        });
        editDescClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextArea(parent_,toBeReplaced);
            TextArea text = (TextArea)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveDescClickHandler != null) 
                    saveDescClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelDescClickHandler != null)
                    cancelDescClickHandler.handle(editBox);
            });
        });
        editStartClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForDatePicker(parent_,toBeReplaced);
            DatePicker datePicker = (DatePicker)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveStartClickHandler != null) 
                    saveStartClickHandler.handle(datePicker);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelStartClickHandler != null)
                    cancelStartClickHandler.handle(editBox);
            });
        });
        editDeadlineClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForDatePicker(parent_,toBeReplaced);
            DatePicker datePicker = (DatePicker)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveDeadlineClickHandler != null) 
                    saveDeadlineClickHandler.handle(datePicker);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelDeadlineClickHandler != null)
                    cancelDeadlineClickHandler.handle(editBox);
            });
        });
        /*********************** END OF CLICK HANDLERS ***********************/
        
        width = dashboard.getView().widthProperty().multiply(0.80).subtract(60);
        
        panel = new VBox();
        panel.prefWidthProperty().bind(width);
        panel.setMinWidth(500);
        panel.setPadding(new Insets(30,40,30,40));
        panel.setAlignment(Pos.TOP_LEFT);
        
        //title
        title = new Label(name);
        title.getStyleClass().add("assessment-title");
        //Type of assessment
        typeLabel = new Label(type);
        typeLabel.getStyleClass().add("assessment-type");
        //Info boxes
        moduleInfo = infoBox("Module:",module);
        moduleInfo.setPadding(new Insets(20,0,0,0));
        moduleInfo.getChildren().get(1).getStyleClass().add("assessment-underline-hover");
        startDateInfo = infoBox("Start Date:",startDate);
        startLbl = (Label)(startDateInfo.getChildren().get(1));
        if(type.equals("Exam")){
            deadlineInfo = infoBox("Date:",deadline);
        }
        else { deadlineInfo = infoBox("Dealine:",deadline); }
        deadlineLbl = (Label)(deadlineInfo.getChildren().get(1));
        weightInfo = infoBox("Weight:",weight + "%");
        descriptionInfo = infoBox("Description:",description);
        descriptionLbl =(Label)(descriptionInfo.getChildren().get(1));
        progressInfo = infoProgressBox("Progress:",progress);
        //Tasks
        tasksLabelBar = taskLabelBar();
        tasksEntries = tasksEntries();
        addTask = new HBox();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        addTask.getChildren().addAll(addTaskButton(),region1);
        addTask.setStyle("-fx-padding: 10 0 0 0");
        
        // Setting on mouse click handlers
        moduleInfo.getChildren().get(1).setOnMouseClicked(event -> {
            if(moduleClickHandler != null) moduleClickHandler.handle(moduleCode);
        });
        title.setOnMouseClicked(event -> {
            if(editNameClickHandler != null) editNameClickHandler.handle(panel,title);
        });
        descriptionInfo.getChildren().get(1).setOnMouseClicked(event -> {
            if(editDescClickHandler != null)
            editDescClickHandler.handle(descriptionInfo,descriptionLbl);
        });
        startDateInfo.getChildren().get(1).setOnMouseClicked(event -> {
            if(editStartClickHandler != null)
            editStartClickHandler.handle(startDateInfo,startLbl);
        });
        deadlineInfo.getChildren().get(1).setOnMouseClicked(event -> {
            if(editDeadlineClickHandler != null)
            editDeadlineClickHandler.handle(deadlineInfo,deadlineLbl);
        });
        
        // If it is an exam don't add startDateInfo
        if(type.equals("Exam")){
            panel.getChildren().addAll(title,typeLabel, moduleInfo);
        }else{
            panel.getChildren().addAll(title,typeLabel, moduleInfo,startDateInfo);
        }
        panel.getChildren().addAll(deadlineInfo,weightInfo,descriptionInfo,progressInfo,
                tasksLabelBar,tasksEntries, addTask);
        setContent(panel);
    }
    
    // Info Box is a HBox of the format   | Label:     Text   |
    public HBox infoBox(String label, String text){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(120.0);
        Label textBox = new Label(text);
        textBox.setMinWidth(150.0);
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, textBox);
        return hbox;
    }
    
    // InfoProgressBox returns a HBox similar to InfoBox but instead of a text
    // in front of the label it shows a progress bar, followed by the value in %
    public HBox infoProgressBox(String label, float progress){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(120.0);
        int percentage = (int)(progress*100);
        Label textBox = new Label(percentage + "%");
        textBox.setPadding(new Insets(0,0,0,10));
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, 
                DeadlinesPane.createProgressBar(progress),textBox);
        return hbox;
    }
    
    // Top row for the tasks table, with a title and a button to add new task on the right
    public HBox taskLabelBar(){
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.getStyleClass().add("assessment-tasks-label-bar");
        hbox.setMaxWidth(700);
        Label tasksLabel = new Label("Tasks");
        tasksLabel.getStyleClass().add("assessment-tasks-label");
        
        HBox addTaskBox = addTaskButton();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        
        hbox.getChildren().addAll(tasksLabel,region1,addTaskBox);
        
        return hbox;
    }
    
    // Button to add a new task
    public HBox addTaskButton(){
        HBox addTaskBox = new HBox();
        addTaskBox.getStyleClass().add("assessment-add-task-box");
        addTaskBox.setAlignment(Pos.CENTER);
        
        ImageView icon = new ImageView();
        icon.getStyleClass().add("dashboard-entry-icon");
        icon.setImage(new Image("icon_add.png"));
        icon.setPreserveRatio(true);
        icon.setFitHeight(24.0);
        
        Label addTaskLabel = new Label("Add new task");
        addTaskLabel.getStyleClass().add("assessment-add-task-label");
        
        addTaskBox.getChildren().addAll(icon,addTaskLabel);
        addTaskBox.setOnMouseClicked(event -> {
                if(addTaskClickHandler != null) addTaskClickHandler.handle();
            });
        
        return addTaskBox;
    }
    
    // A VBox containing all the task entries of the aseessment
    public VBox tasksEntries(){
        VBox tasksEntries = new VBox();
        tasksEntries.setMaxWidth(700);
        /* For each task, an entry shows the task name on the left, and it progress
         * bar on the right. An entry can be click to take to the TaskView of
         * the respective task*/
        for(TaskInfo t : tasks){
            HBox entry = new HBox();
            entry.setAlignment(Pos.CENTER_LEFT);
            entry.getStyleClass().add("assessment-task-entry");
            Label taskLabel = new Label(t.getName());
            taskLabel.getStyleClass().add("assessment-task-entry-text");
            Region region1 = new Region();
            HBox.setHgrow(region1, Priority.ALWAYS);
            entry.getChildren().addAll(taskLabel, region1,
                    DeadlinesPane.createProgressBar(t.getProgress()));
            entry.setOnMouseClicked(event -> {
                if(taskClickHandler != null) taskClickHandler.handle(t.getName());
            });
            tasksEntries.getChildren().add(entry);
        }
        return tasksEntries;
    }
    
    /* Replaces an element parsed as argument for a TextField to allow user input
     * so an user can edit a certain field */
    public HBox replaceForTextField(Pane parent, Node toReplace, float width){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the text of the TextField as the text the label replaced
        if (toReplace instanceof Label) {
            Label lbl = (Label)toReplace;
            TextField text = new TextField(lbl.getText());
            box.getChildren().add(text); 
            text.setPrefWidth(width);
            if(lbl.getText()!=null && lbl.getText().equals(name)){
                text.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
            }
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    // replaceForTextField with a default width of 500px
    public HBox replaceForTextField(Pane parent, Node toReplace){
        return replaceForTextField(parent, toReplace, 500f);
    }
    
    /* Replaces an element parsed as argument for a TextArea to allow user input
     * so an user can edit a certain field (for description) */
    public HBox replaceForTextArea(Pane parent, Node toReplace, float width, float height){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the text of the TextArea as the text the label replaced
        if (toReplace instanceof Label) {
            Label lbl = (Label)toReplace;
            TextArea text = new TextArea(lbl.getText());
            box.getChildren().add(text); 
            text.setPrefWidth(width);
            text.setPrefHeight(height);
            text.setWrapText(true);
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    // replaceForTextArea with a default size of 500x100px
    public HBox replaceForTextArea(Pane parent, Node toReplace){
        return replaceForTextArea(parent,toReplace,500f,100f);
    }
    
    /* Replaces an element parsed as argument for a DatePicker to allow user input
     * so an user can edit a certain date field */
    public HBox replaceForDatePicker(Pane parent, Node toReplace){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the date of the DatePicker as the date in the label replaced
        if (toReplace instanceof Label) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            Label lbl = (Label)toReplace;
            LocalDate date = LocalDate.parse(lbl.getText(),formatter);
            DatePicker dp = new DatePicker(date);
            box.getChildren().add(dp); 
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    
    public interface ModuleClickHandler{
        void handle(String ModuleCode);
    }
    public interface TaskClickHandler{
        void handle(String TaskName);
    }
    public interface AddTaskClickHandler{
        void handle();
    }
    public interface EditClickHandler{
        void handle(Pane parent, Node toReplace);
    }
    public interface CancelClickHandler{
        void handle(HBox editBox);
    }
    public interface SaveTxtFieldClickHandler{
        void handle(TextField newInfo);
    }
    public interface SaveTxtAreaClickHandler{
        void handle(TextArea newInfo);
    }
    public interface SaveDateClickHandler{
        void handle(DatePicker newInfo);
    }
    
    public void addTask(TaskInfo task){
        tasks.add(task);
    }
    public void setName(String name) { this.name = name; }
    public void setType(String type) { this.type = type; }
    public void setModule(String module) { this.module = module; }
    public void setModuleCode(String moduleCode) { this.moduleCode = moduleCode; }
    public void setStartDate(String startDate) { this.startDate = startDate; }
    public void setDeadline(String deadline) { this.deadline = deadline; }
    public void setDescription(String description)
        { this.description = description; }
    public void setWeight(int weight) { this.weight = weight; }
    public void setProgress(float progress) { this.progress = progress; }
    
    
}
