package view.assessment;

import java.util.ArrayList;
import java.util.HashMap;
import model.StudyActivity.Type;

/**
 * Helper class to store information of a task needed for displaying in the
 * AssessmentView, TaskView and ActivityView
 */
public class TaskInfo {
    private String name, description, assessment;
    private int timeToSpend;
    private float progress;
    private ArrayList<TaskInfo> requiredTasks;
    private ArrayList<NoteInfo> notes;
    private ArrayList<ActivityInfo> activities;

    public TaskInfo(String name, String description, String assessment, 
            int timeToSpend, float progress) {
        this.name = name;
        this.description = description;
        this.assessment = assessment;
        this.timeToSpend = timeToSpend;
        this.progress = progress;
        requiredTasks = new ArrayList<>();
        notes = new ArrayList<>();
        activities = new ArrayList<>();
        
    }

    public String getName() { return name; }
    public String getDescription() { return description; }
    public String getAssessment() { return assessment; }
    public int getTimeToSpend() { return timeToSpend; }
    public float getProgress() { return progress; }
    public ArrayList<NoteInfo> getNotes() { return notes; }
    public ArrayList<TaskInfo> getRequiredTasks() { return requiredTasks; }
    public ArrayList<ActivityInfo> getActivities() { return activities; }

    
    public void setName(String name) { this.name = name; }
    public void setDescription(String description)
        { this.description = description; }
    public void setAssessment(String assessment)
        { this.assessment = assessment; }
    public void setTimeToSpend(int timeToSpend)
        { this.timeToSpend = timeToSpend;}
    public void setProgress(float progress) { this.progress = progress; }
    
    public void addRequiredTask(TaskInfo task){
        requiredTasks.add(task);
    }
    public void addNote(NoteInfo note){
        notes.add(note);
    }
    public void addActivity(ActivityInfo activity){
        activities.add(activity);
    }
    
/**
* Helper class to store information of a note needed for displaying in the
* TaskView and ActivityView
*/
    public static class NoteInfo {
        private String note, date;
        
        public NoteInfo(String note, String date) {
            this.note = note;
            this.date = date;
        }

        public String getNote() { return note; }
        public String getDate() { return date; }

        public void setNote(String note) { this.note = note; }
        public void setDate(String date) { this.date = date; }
    }
    
/**
* Helper class to store information of an activity needed for displaying in the
* TaskView and ActivityView
*/
    public static class ActivityInfo {
        private static HashMap<String,String> types;
        
        private String name, type, targetUnits, assessment;
        private float progress;
        private int target, completed;
        private ArrayList<TaskInfo> tasks;
        private ArrayList<NoteInfo> notes;

        public ActivityInfo(String name, String type, float progress,
                int target, int completed, String targetUnits) {
            this.tasks = new ArrayList<>();
            this.notes = new ArrayList<>();
            this.name = name;
            this.type = type;
            this.progress = progress;
            this.target = target;
            this.completed = completed;
            this.targetUnits = targetUnits;
        }

        public String getName() { return name; }
        public String getType() { return type; }
        public float getProgress() { return progress; }
        public int getTarget() { return target; }
        public int getCompleted() { return completed; }
        public ArrayList<TaskInfo> getTasks() { return tasks; }
        public ArrayList<NoteInfo> getNotes() { return notes; }
        public String getUnits() { return targetUnits; }
        public String getAssessment() { return assessment; }

        public void setName(String name) { this.name = name; }
        public void setType(String type) { this.type = type; }
        public void setProgress(float progress) { this.progress = progress; }
        public void setTarget(int target) { 
            this.target = target;
            this.progress = (float)completed/(float)this.target;
        }
        public void setCompleted(int completed) { 
            this.completed = completed; 
            this.progress = (float)this.completed/(float)target;
        }
        public void setAssessment(String assessment){
            this.assessment = assessment;
        }
        public void addTask(TaskInfo task){
            tasks.add(task);
        }
        public void addNote(NoteInfo note){
            notes.add(note);
        }
        
        // HashMap that links Type to its units (both as Strings)
        public static HashMap<String,String> getTypes(){
            if(types==null){
                types = new HashMap();
                for(Type t : Type.values()){
                    types.put(t.toString(), t.getUnits());
                }
            }
            return types;
        }
    }
    
    
}
