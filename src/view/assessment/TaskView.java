package view.assessment;

import controller.DashboardController;
import controller.TaskController;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.AddActivity;
import view.AddNote;
import view.AlertBox;
import view.ConfirmBox;
import static view.assessment.ActivityView.deleteIcon;
import view.assessment.TaskInfo.*;

import view.deadlines.DeadlinesPane;

/**
 * Task View
 * 
 * Displays the information of a specific task, including a list of all its 
 * activities and notes
 * 
 */
public class TaskView extends ScrollPane{
    private TaskInfo task;
    private TaskController controller;
    
    private VBox panel;
    private DoubleBinding width;
    
    private AssessmentClickHandler assessmentClickHandler;
    private ActivityClickHandler activityClickHandler;
    private TaskClickHandler taskClickHandler;
    private DeleteClickHandler deleteClickHandler;
    private AddActivityClickHandler addActivityClickHandler;
    private SaveComboClickHandler saveTaskAddedClickHandler;
    private AddNoteClickHandler addNoteClickHandler;
    private DeleteNoteClickHandler deleteNoteClickHandler;
    private final EditClickHandler editNameClickHandler, editDescClickHandler,
            editDurationClickHandler, addReqTaskClickHandler;
    private final CancelClickHandler cancelNameClickHandler, cancelDescClickHandler,
            cancelDurationClickHandler, cancelAddTaskClickHandler;
    private final SaveTxtFieldClickHandler saveNameClickHandler, 
            saveDurationClickHandler;
   private final SaveTxtAreaClickHandler saveDescClickHandler;
   private final RemoveReqTaskClickHandler removeReqTaskClickHandler;
    
    private Label title, taskLabel, descriptionLbl, durationLbl, addTastLbl;
    private Button deleteButton;
    private HBox assessmentInfo, descriptionInfo, durationInfo, progressInfo,
            activitiesLabelBar, addActivity, notesLabelBar, addNote, titleAndDelete;
    private VBox activitiesEntries, tasksBox;
    private FlowPane noteEntries;
    
    public TaskView(DashboardController parent, String assessmentName, String taskName) {
        getStyleClass().add("assessment");
        controller = new TaskController(parent,this, assessmentName, taskName);
        
        /****************** CHANGE VIEW CLICK HANDLERS ***********************/
        assessmentClickHandler = (() -> {
            parent.getView().setContentView(new AssessmentView(parent, assessmentName));
        });
        activityClickHandler = (activName -> {
            parent.getView().setContentView(new ActivityView(parent, assessmentName,taskName,activName) );
        });
        taskClickHandler = ((task_name) -> {
            parent.getView().setContentView(new TaskView(parent,assessmentName, task_name) );
        });
        deleteClickHandler = (() -> {
            boolean confirm = ConfirmBox.display("Delete Task","Deleting a task is irreversible."
                    + " All activities that are solely associated with this task will also be deleted."
                    + "\nAre you sure you want to delete the task?");
            if(confirm){
                controller.deleteTask();
                parent.getView().setContentView(new AssessmentView(parent, assessmentName));
            }
        });
        // Opens up a AddActivity pop-up window
        addActivityClickHandler = (() -> {
            ActivityInfo newActivity = AddActivity.display(task.getName());
            if(newActivity!=null){
                addActivityEntry(newActivity);
            }
        });
        // Opens up a AddNote pop-up window
        addNoteClickHandler = (() -> {
            String newNote = AddNote.display(task.getName(), 1);
            if(newNote!=null){
                addNoteEntry(newNote);
            }
        });
        deleteNoteClickHandler = ((VBox entry) -> {
            Text noteText = (Text)entry.getChildren().get(1);
            String note = noteText.getText();
            if(controller.removeNote(note)){
                removeNoteEntry(entry,note);
            }
        });
        /******************** SAVE BUTTONS HANDLERS ***************************/
        /* Each save button must call the controller to try and set the respective
         * field to the value the user entered, and if successful, it must replace
         * input/edit box for a label with the new value */
        saveNameClickHandler = (field -> {
            if(controller.setName(field.getText())){
                task.setName(field.getText());
                title.setText(task.getName());
                titleAndDelete.getChildren().remove(field.getParent());
                titleAndDelete.getChildren().add(0, title);
            }
        });
        saveDescClickHandler = (area -> {
            controller.setDescription(area.getText());
            task.setDescription(area.getText());
            descriptionLbl.setText(task.getDescription());
            descriptionInfo.getChildren().remove(area.getParent());
            descriptionInfo.getChildren().add(1, descriptionLbl);
        });
        saveDurationClickHandler = (field -> {      
            int days;
            try { 
                days = Integer.parseInt(field.getText());
                if(controller.setTimeToSpend(days)){
                    task.setTimeToSpend(days);
                    durationLbl.setText(task.getTimeToSpend() + " days");
                    durationInfo.getChildren().remove(field.getParent());
                    durationInfo.getChildren().add(1, durationLbl);
                }
            }
            catch (NumberFormatException e) {
               AlertBox.display("Invalid entry","You must enter a number.");
            }
            
        });
        saveTaskAddedClickHandler = (combo -> {
            System.out.println("saved!!!");
            if(combo.getValue()!=null){
                TaskInfo t = controller.addRequiredTask(combo.getValue().toString());
                if(t!=null){
                    task.addRequiredTask(t);
                    tasksBox.getChildren().remove(combo.getParent());
                    tasksBox.getChildren().addAll(requiredTaskEntry(t), addTastLbl);
                }
            }
            else{
                tasksBox.getChildren().remove(combo.getParent());
                tasksBox.getChildren().addAll(addTastLbl);
            }
        });
        removeReqTaskClickHandler = ((HBox entry) -> {
            System.out.println("delete task pls");
            Label taskLbl = (Label)entry.getChildren().get(0);
            String taskName_ = taskLbl.getText();
            if(controller.removeRequiredTask(taskName_)){
                removeTaskEntry(entry,taskName_);
            }
        });
        
        /******************** CANCEL BUTTONS HANDLERS *************************/
        /* Cancel buttons simply replace the input/edit box with the label with
         * the old value */
        cancelNameClickHandler = ((editBox) -> {
            titleAndDelete.getChildren().remove(editBox);
            titleAndDelete.getChildren().add(0, title);
        });
        cancelDescClickHandler = ((editBox) -> {
            descriptionInfo.getChildren().remove(editBox);
            descriptionInfo.getChildren().add(1, descriptionLbl);
        });
        cancelDurationClickHandler = ((editBox) -> {
            durationInfo.getChildren().remove(editBox);
            durationInfo.getChildren().add(1, durationLbl);
        });
        cancelAddTaskClickHandler = ((editBox) -> {
            tasksBox.getChildren().remove(editBox);
            tasksBox.getChildren().add(addTastLbl);
        });
        /****************** EDIT INFORMATION HANDLERS**************************/
        /* Edit click handler must replace the label of the field clicked
         * (if editable) with an appropriate input/edit box, and save and cancel buttons*/
        editNameClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced);
            TextField text = (TextField)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveNameClickHandler != null)
                    saveNameClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelNameClickHandler != null)
                    cancelNameClickHandler.handle(editBox);
            });
        });
        editDescClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextArea(parent_,toBeReplaced);
            TextArea text = (TextArea)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveDescClickHandler != null) 
                    saveDescClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelDescClickHandler != null)
                    cancelDescClickHandler.handle(editBox);
            });
        });
        editDurationClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced,60);
            TextField text = (TextField)editBox.getChildren().get(0);
            text.setText(text.getText().replace(" days", ""));
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            Label unitsLbl = new Label("days");
            unitsLbl.getStyleClass().add("assessment-info");
            editBox.getChildren().add(1,unitsLbl);
            save.setOnMouseClicked(event -> {
                if(saveDurationClickHandler != null) 
                    saveDurationClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelDurationClickHandler != null)
                    cancelDurationClickHandler.handle(editBox);
            });
        });
        addReqTaskClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTasksComboBox(parent_,toBeReplaced);
            ComboBox combo = (ComboBox)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveTaskAddedClickHandler != null) 
                    saveTaskAddedClickHandler.handle(combo);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelAddTaskClickHandler != null)
                    cancelAddTaskClickHandler.handle(editBox);
            });
        });
        /*********************** END OF CLICK HANDLERS ***********************/
        
        width = parent.getView().widthProperty().multiply(0.80).subtract(60);
        
        panel = new VBox();
        panel.prefWidthProperty().bind(width);
        panel.setMinWidth(500);
        panel.setPadding(new Insets(30,40,30,40));
        panel.setAlignment(Pos.TOP_LEFT);
        
        Region region2 = new Region();
        HBox.setHgrow(region2, Priority.ALWAYS);
        //title
        title = new Label(task.getName());
        title.getStyleClass().add("assessment-title");
        deleteButton = new Button("Delete");
        deleteButton.getStyleClass().add("assessment-delete-task");
        titleAndDelete = new HBox();
        titleAndDelete.getChildren().addAll(title, region2, deleteButton);
        titleAndDelete.setMaxWidth(700);
        //Task label
        taskLabel = new Label("Task");
        taskLabel.getStyleClass().add("assessment-type");
        //Info boxes
        assessmentInfo = infoBox("Assessment:",task.getAssessment());
        assessmentInfo.setPadding(new Insets(20,0,0,0));
        assessmentInfo.getChildren().get(1).getStyleClass()
                                    .add("assessment-underline-hover");
        descriptionInfo = infoBox("Description:",task.getDescription());
        descriptionLbl =(Label)(descriptionInfo.getChildren().get(1));
        durationInfo = infoBox("Duration:",task.getTimeToSpend() + " days");
        durationLbl =(Label)(durationInfo.getChildren().get(1));
        progressInfo = infoProgressBox("Progress:",task.getProgress());
        //Activities
        activitiesLabelBar = activitiesLabelBar();
        activitiesEntries = activitiesEntries();
        addActivity = new HBox();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        addActivity.getChildren().addAll(addActivityButton(),region1);
        addActivity.setStyle("-fx-padding: 10 0 0 0");
        //Notes
        notesLabelBar = notesLabelBar();
        noteEntries = notesEntries();
        addNote = new HBox();
        HBox.setHgrow(region1, Priority.ALWAYS);
        addNote.getChildren().addAll(addNoteButton(),region1);
        addNote.setStyle("-fx-padding: 10 0 0 0");
        
        // Setting on mouse click handlers
       assessmentInfo.getChildren().get(1).setOnMouseClicked(event -> {
                if(assessmentClickHandler != null)
                    assessmentClickHandler.handle();
            });
       title.setOnMouseClicked(event -> {
            if(editNameClickHandler != null) editNameClickHandler.handle(titleAndDelete,title);
        });
       deleteButton.setOnAction(event -> {
            if(deleteClickHandler != null) deleteClickHandler.handle();
        });
       addNote.setOnMouseClicked(event -> {
            if(addNoteClickHandler != null) addNoteClickHandler.handle();
        });
        notesLabelBar.getChildren().get(2).setOnMouseClicked(event -> {
            if(addNoteClickHandler != null) addNoteClickHandler.handle();
        });
       descriptionLbl.setOnMouseClicked(event -> {
            if(editDescClickHandler != null)
            editDescClickHandler.handle(descriptionInfo,descriptionLbl);
        });
       durationLbl.setOnMouseClicked(event -> {
            if(editDurationClickHandler != null)
            editDurationClickHandler.handle(durationInfo,durationLbl);
        });
       
        
        panel.getChildren().addAll(titleAndDelete,taskLabel,assessmentInfo,
                descriptionInfo,durationInfo,progressInfo,requiredTasksBox(),
                activitiesLabelBar,activitiesEntries,addActivity,
                notesLabelBar,noteEntries,addNote);
        setContent(panel);
    
    }
    
    // Info Box is a HBox of the format   | Label:     Text   |
    public HBox infoBox(String label, String text){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(140.0);
        Label textBox = new Label(text);
        textBox.setMinWidth(140.0);
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, textBox);
        return hbox;
    }
    
    // InfoProgressBox returns a HBox similar to InfoBox but instead of a text
    // in front of the label it shows a progress bar, followed by the value in %
    public HBox infoProgressBox(String label, float progress){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(140.0);
        int percentage = (int)(progress*100);
        Label textBox = new Label(percentage + "%");
        textBox.setPadding(new Insets(0,0,0,10));
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, DeadlinesPane.createProgressBar(progress),textBox);
        return hbox;
    }
    
    // Similar to InfoBox, but the right side is a VBox listing all the required
    // tasks (with additional line to add more required tasks)
    public HBox requiredTasksBox(){
        HBox hbox = new HBox();
        Label labelBox = new Label("Required Tasks:");
        labelBox.setPrefWidth(140.0);
        tasksBox = new VBox();
        tasksBox.setMinWidth(140.0);
        labelBox.getStyleClass().add("assessment-info");
        for(TaskInfo t : task.getRequiredTasks()){
            HBox entry = requiredTaskEntry(t);
            tasksBox.getChildren().add(entry);
        }
        addTastLbl = new Label("(+) Add a required task");
        addTastLbl.getStyleClass().add("assessment-info");
        addTastLbl.getStyleClass().add("assessment-underline-hover");
        addTastLbl.setStyle("-fx-font-size: 12px; ");
        addTastLbl.setOnMouseClicked(event -> {
            if(addReqTaskClickHandler != null)
            addReqTaskClickHandler.handle(tasksBox,addTastLbl);
        });
        tasksBox.getChildren().add(addTastLbl);
        hbox.getChildren().addAll(labelBox, tasksBox);
        return hbox;
    }
    // A single entry for requiredTasksBox. It shows the name of the task (clickable
    // to take to that task's view), and a X symbol to be remove task from required tasks
    public HBox requiredTaskEntry(TaskInfo t){
        HBox entry = new HBox();
        Label entryLbl = new Label(t.getName());
        entryLbl.getStyleClass().add("assessment-info");
        entryLbl.getStyleClass().add("assessment-underline-hover");
        entryLbl.setPadding(new Insets(0,5,0,0));
        entryLbl.setOnMouseClicked(event -> {
            if(taskClickHandler != null)
                taskClickHandler.handle(t.getName());
        });
        ImageView icon_delete = deleteIcon();
        icon_delete.getStyleClass().add("dashboard-delete-item");
        icon_delete.setOnMouseClicked(event -> {
            if(removeReqTaskClickHandler != null) removeReqTaskClickHandler.handle(entry);
        });
        
        entry.setAlignment(Pos.CENTER_LEFT);
        entry.getChildren().addAll(entryLbl,icon_delete);
        return entry;
    }
    // Method to remove a required task, removing the respective entry from the list
    private void removeTaskEntry(HBox entry, String taskName_) {
        for(TaskInfo t : task.getRequiredTasks()){
            if(t.getName().equals(taskName_)){
                task.getRequiredTasks().remove(t);
                VBox parent = (VBox)entry.getParent();
                parent.getChildren().remove(entry);
            }
        }
    }
    
    // Top row for the activities table, with a title and a button to add new
    // activity on the right
    public HBox activitiesLabelBar(){
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.getStyleClass().add("assessment-tasks-label-bar");
        hbox.setMaxWidth(700);
        Label activitiesLabel = new Label("Activities");
        activitiesLabel.getStyleClass().add("assessment-tasks-label");
        
        HBox addActivityBox = addActivityButton();
        addActivityBox.setOnMouseClicked(event -> {
            if(addActivityClickHandler != null)
            addActivityClickHandler.handle();
        });
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        
        hbox.getChildren().addAll(activitiesLabel,region1,addActivityBox);
        
        return hbox;
    }
    
    // Button to add a new activity
    public HBox addActivityButton(){
        HBox addActivityBox = new HBox();
        addActivityBox.getStyleClass().add("assessment-add-task-box");
        addActivityBox.setAlignment(Pos.CENTER);
        
        ImageView icon = new ImageView();
        icon.getStyleClass().add("dashboard-entry-icon");
        icon.setImage(new Image("icon_add.png"));
        icon.setPreserveRatio(true);
        icon.setFitHeight(24.0);
        
        Label addActivityLabel = new Label("Add new activity");
        addActivityBox.setOnMouseClicked(event -> {
            if(addActivityClickHandler != null)
            addActivityClickHandler.handle();
        });
        addActivityLabel.getStyleClass().add("assessment-add-task-label");
        
        addActivityBox.getChildren().addAll(icon,addActivityLabel);
        
        return addActivityBox;
    }
    // A VBox containing all the activities entries of the task
    public VBox activitiesEntries(){
        VBox entriesBox = new VBox();
        entriesBox.setMaxWidth(700);
        for(ActivityInfo a : task.getActivities()){
            HBox entry = activityEntry(a);
            entriesBox.getChildren().add(entry);
        }
        return entriesBox;
    }
    /* Entry of a single activity: shows the task name on the left, and it progress
    * bar on the right. An entry can be click to take to the ActivityView of
    * the respective activity*/
    public HBox activityEntry(ActivityInfo a){
        HBox entry = new HBox();
        entry.setAlignment(Pos.CENTER_LEFT);
        entry.getStyleClass().add("assessment-task-entry");
        Label activityLabel = new Label(a.getName());
        activityLabel.getStyleClass().add("assessment-task-entry-text");
        activityLabel.setStyle("-fx-padding: 0 10 0 0");
        Label typeLabel = new Label(a.getType());
        typeLabel.getStyleClass().add("assessment-type");
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        entry.getChildren().addAll(activityLabel, typeLabel, region1,
                DeadlinesPane.createProgressBar(a.getProgress()));
        entry.setOnMouseClicked(event -> {
            if(activityClickHandler != null) 
                activityClickHandler.handle(a.getName());
        });
        return entry;
    }
    
    // Method to add an activity, and displaying it on the view
    public void addActivityEntry(ActivityInfo activity){
        boolean added = controller.addActivity(activity);
        
        if(added){
            task.addActivity(activity);
            HBox entry = activityEntry(activity);
            activitiesEntries.getChildren().add(entry);
        }
    }
    
    // Top row for the notes entries, with a title and a button to add new note on the right
    public HBox notesLabelBar(){
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.getStyleClass().add("assessment-tasks-label-bar");
        hbox.setMaxWidth(700);
        Label activitiesLabel = new Label("Notes");
        activitiesLabel.getStyleClass().add("assessment-tasks-label");
        
        HBox addNoteBox = addNoteButton();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        
        hbox.getChildren().addAll(activitiesLabel,region1,addNoteBox);
        
        return hbox;
    }
    
    // Button to add a new note
    public HBox addNoteButton(){
        HBox addNoteBox = new HBox();
        addNoteBox.getStyleClass().add("assessment-add-task-box");
        addNoteBox.setAlignment(Pos.CENTER);
        
        ImageView icon = new ImageView();
        icon.getStyleClass().add("dashboard-entry-icon");
        icon.setImage(new Image("icon_add.png"));
        icon.setPreserveRatio(true);
        icon.setFitHeight(24.0);
        
        Label addNoteLabel = new Label("Add new note");
        addNoteLabel.getStyleClass().add("assessment-add-task-label");
        
        addNoteBox.getChildren().addAll(icon,addNoteLabel);
        
        return addNoteBox;
    }
    
    // Note entries are displayed in a Flowpane
    public FlowPane notesEntries(){
        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(10);
        flow.setHgap(10);
        flow.setPrefWrapLength(700); 

        for(NoteInfo n : task.getNotes()){
            VBox entry = noteEntry(n);
            flow.getChildren().add(entry);
        }
        return flow;
    }
    
    // Each note entry displays the date on top, followed by the note itself
    public VBox noteEntry(NoteInfo n){
        ImageView icon_delete = deleteIcon();
        
        Region reg = new Region();
        HBox.setHgrow(reg, Priority.ALWAYS);
        
        VBox entry = new VBox();
        entry.getStyleClass().add("assessment-note-entry");
        entry.setPrefWidth(225);
        entry.setMaxWidth(225);
        Label dateLabel = new Label(n.getDate());
        dateLabel.getStyleClass().add("assessment-task-entry-text");
        dateLabel.setStyle("-fx-padding: 0 0 5 0");
        Text noteText = new Text(n.getNote());
        noteText.setWrappingWidth(205);
        noteText.getStyleClass().add("assessment-task-entry-text");
        entry.getChildren().addAll(new HBox(dateLabel, reg,icon_delete), noteText);
        
        icon_delete.setOnMouseClicked(event -> {
            if(deleteNoteClickHandler != null) deleteNoteClickHandler.handle(entry);
        });
        
        return entry;
    }
    // Method to add a note, and displaying it on the view
    public void addNoteEntry(String note){
        NoteInfo noteInfo = controller.addNote(note);
        
        if(noteInfo!=null){
            task.addNote(noteInfo);
            VBox entry = noteEntry(noteInfo);

            noteEntries.getChildren().add(entry);
        }
    }
    // Method to remove a note, and removing it from the view
    private void removeNoteEntry(VBox entry, String note) {
        for(NoteInfo n : task.getNotes()){
            if(n.getNote().equals(note)){
                FlowPane parent = (FlowPane)entry.getParent();
                parent.getChildren().remove(entry);
            }
        }
    }
    
    /* Replaces an element parsed as argument for a TextField to allow user input
     * so an user can edit a certain field */
    public HBox replaceForTextField(Pane parent, Node toReplace, float width){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the text of the TextField as the text the label replaced
        if (toReplace instanceof Label) {
            Label lbl = (Label)toReplace;
            TextField text = new TextField(lbl.getText());
            box.getChildren().add(text); 
            text.setPrefWidth(width);
            if(lbl.getText()!=null && lbl.getText().equals(task.getName())){
                text.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
            }
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    // replaceForTextField with a default width of 500px
    public HBox replaceForTextField(Pane parent, Node toReplace){
        return replaceForTextField(parent, toReplace, 500f);
    }
    /* Replaces an element parsed as argument for a TextArea to allow user input
     * so an user can edit a certain field (for description) */
    public HBox replaceForTextArea(Pane parent, Node toReplace, float width, float height){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the text of the TextArea as the text the label replaced
        if (toReplace instanceof Label) {
            Label lbl = (Label)toReplace;
            TextArea text = new TextArea(lbl.getText());
            box.getChildren().add(text); 
            text.setPrefWidth(width);
            text.setPrefHeight(height);
            text.setWrapText(true);
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    // replaceForTextArea with a default size of 500x100px
    public HBox replaceForTextArea(Pane parent, Node toReplace){
        return replaceForTextArea(parent,toReplace,500f,100f);
    }
    /* Replaces an element parsed as argument for a ComboBox to allow user input
     * so an user can choose from limit set of options (for adding required tasks) */
    
    public HBox replaceForTasksComboBox(Pane parent, Node toReplace){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        if (toReplace instanceof Label) {
            ComboBox optionBox = new ComboBox();
            for(String option : controller.getTaskOptions()){
                optionBox.getItems().add(option);
            }
            box.getChildren().add(optionBox); 
            optionBox.setPrefWidth(400);
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }

    public interface AssessmentClickHandler{
        void handle();
    }
    public interface ActivityClickHandler{
        void handle(String ActivityName);
    }
    public interface TaskClickHandler{
        void handle(String TaskName);
    }
    public interface DeleteClickHandler{
        void handle();
    }
    public interface AddActivityClickHandler{
        void handle();
    }
    public interface RemoveReqTaskClickHandler{
        void handle(HBox entry);
    }
    public interface DeleteNoteClickHandler{
        void handle(VBox entry);
    }
    public interface AddNoteClickHandler{
        void handle();
    }
    public interface EditClickHandler{
        void handle(Pane parent, Node toReplace);
    }
    public interface CancelClickHandler{
        void handle(HBox editBox);
    }
    public interface SaveTxtFieldClickHandler{
        void handle(TextField newInfo);
    }
    public interface SaveTxtAreaClickHandler{
        void handle(TextArea newInfo);
    }
    public interface SaveComboClickHandler{
        void handle(ComboBox newInfo);
    }
    public void setTask(TaskInfo task) { this.task = task; }
}
