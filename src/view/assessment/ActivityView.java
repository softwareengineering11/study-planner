/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.assessment;

import controller.ActivityController;
import controller.DashboardController;
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import view.AddNote;
import view.AlertBox;
import view.ConfirmBox;
import view.assessment.TaskInfo.*;
import view.deadlines.DeadlinesPane;

/**
 *
 * @author Alice
 */
public class ActivityView extends ScrollPane{
    private ActivityInfo activity;
    private ActivityController controller;
    
    private VBox panel;
    private DoubleBinding width;
    
    private AssessmentClickHandler assessmentClickHandler;
    private TaskClickHandler taskClickHandler;
    private DeleteClickHandler deleteClickHandler;
    private AddNoteClickHandler addNoteClickHandler;
    private DeleteNoteClickHandler deleteNoteClickHandler;
    private final EditClickHandler editNameClickHandler, editProgressClickHandler,
            editTargetClickHandler, addTaskClickHandler;
    private final CancelClickHandler cancelNameClickHandler, cancelProgressClickHandler,
            cancelTargetClickHandler, cancelAddTaskClickHandler;
    private final SaveTxtFieldClickHandler saveNameClickHandler,
            saveProgressClickHandler, saveTargetClickHandler;
    private final SaveComboClickHandler saveTaskAddedClickHandler;
    private final RemoveTaskClickHandler removeTaskClickHandler;
    
    private Label title, typeLabel,progressLbl,targetLbl, typeInfoLbl, addTastLbl;
    private HBox assessmentInfo, typeInfo, progressBarInfo, progressInfo, tasksInfo,
            notesLabelBar, addNote, titleAndDelete;
    private FlowPane noteEntries;
    private VBox tasksBox;
    private Button deleteButton;
    
    public ActivityView(DashboardController parent, String assessmentName, String taskName, String activityName) {
        getStyleClass().add("assessment");
        controller = new ActivityController(parent,this, assessmentName,
                taskName, activityName);
        
        /****************** CHANGE VIEW CLICK HANDLERS ***********************/
        assessmentClickHandler = (() -> {
            parent.getView().setContentView(new AssessmentView(parent, assessmentName));
        });
        taskClickHandler = ((task_name) -> {
            parent.getView().setContentView(new TaskView(parent, assessmentName, task_name) );
        });
        deleteClickHandler = (() -> {
            boolean confirm = ConfirmBox.display("Delete ACtivity","Deleting an activity is irreversible."
                    + " the activity will be deleted from all tasks it is associated with."
                    + "\nAre you sure you want to delete the activity?");
            if(confirm){
                controller.deleteActivity();
                parent.getView().setContentView(new AssessmentView(parent, assessmentName));
            }
        });
        // Opens up a AddNote pop-up window
        addNoteClickHandler = (() -> {
            String newNote = AddNote.display(activity.getName(), 0);
            if(newNote!=null){
                addNoteEntry(newNote);
            }
        });
        deleteNoteClickHandler = ((VBox entry) -> {
            Text noteText = (Text)entry.getChildren().get(1);
            String note = noteText.getText();
            if(controller.removeNote(note)){
                removeNoteEntry(entry,note);
            }
        });
        removeTaskClickHandler = ((HBox entry) -> {
            Label taskLbl = (Label)entry.getChildren().get(0);
            String taskName_ = taskLbl.getText();
            if(controller.removeTask(taskName_)){
                removeTaskEntry(entry,taskName_);
            }
        });
        /******************** SAVE BUTTONS HANDLERS ***************************/
        /* Each save button must call the controller to try and set the respective
         * field to the value the user entered, and if successful, it must replace
         * input/edit box for a label with the new value */
        saveNameClickHandler = (field -> {
            if(controller.setName(field.getText())){
                activity.setName(field.getText());
                title.setText(activity.getName());
                titleAndDelete.getChildren().remove(field.getParent());
                titleAndDelete.getChildren().add(0, title);
            }
        });
        saveProgressClickHandler = (field -> {      
            int integer;
            try { 
                integer = Integer.parseInt(field.getText());
                if(controller.setCompleted(integer)){
                    activity.setCompleted(integer);
                    progressLbl.setText(activity.getCompleted() + "  /  ");
                    progressInfo.getChildren().remove(field.getParent());
                    progressInfo.getChildren().add(1, progressLbl);
                    panel.getChildren().remove(progressBarInfo);
                    progressBarInfo = infoProgressBox("Progress:",activity.getProgress());
                    panel.getChildren().add(4,progressBarInfo);
                }
            }
            catch (NumberFormatException e) {
               AlertBox.display("Invalid entry","You must enter a number.");
            }
        });
        saveTargetClickHandler = (field -> {      
            int integer;
            try { 
                integer = Integer.parseInt(field.getText());
                if(controller.setTarget(integer)){
                    activity.setTarget(integer);
                    targetLbl.setText(activity.getTarget() + "  " + activity.getUnits());
                    progressInfo.getChildren().remove(field.getParent());
                    progressInfo.getChildren().add(targetLbl);
                    panel.getChildren().remove(progressBarInfo);
                    progressBarInfo = infoProgressBox("Progress:",activity.getProgress());
                    panel.getChildren().add(4,progressBarInfo);
                }
            }
            catch (NumberFormatException e) {
               AlertBox.display("Invalid entry","You must enter a number.");
            }
        });
        saveTaskAddedClickHandler = (combo -> {
            if(combo.getValue()!=null){
                TaskInfo t = controller.addTask(combo.getValue().toString());
                if(t!=null){
                    activity.addTask(t);
                    tasksBox.getChildren().remove(combo.getParent());
                    tasksBox.getChildren().addAll(taskEntry(t), addTastLbl);
                }
            }
            else{
                tasksBox.getChildren().remove(combo.getParent());
                    tasksBox.getChildren().addAll(addTastLbl);
            }
        });
        /******************** CANCEL BUTTONS HANDLERS *************************/
        /* Cancel buttons simply replace the input/edit box with the label with
         * the old value */
        cancelNameClickHandler = ((editBox) -> {
            titleAndDelete.getChildren().remove(editBox);
            titleAndDelete.getChildren().add(0, title);
        });
        cancelProgressClickHandler = ((editBox) -> {
            progressInfo.getChildren().remove(editBox);
            progressInfo.getChildren().add(1, progressLbl);
        });
        cancelTargetClickHandler = ((editBox) -> {
            progressInfo.getChildren().remove(editBox);
            progressInfo.getChildren().add(targetLbl);
        });
        cancelAddTaskClickHandler = ((editBox) -> {
            tasksBox.getChildren().remove(editBox);
            tasksBox.getChildren().add(addTastLbl);
        });
        /****************** EDIT INFORMATION HANDLERS**************************/
        /* Edit click handler must replace the label of the field clicked
         * (if editable) with an appropriate input/edit box, and save and cancel buttons*/
        editNameClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced);
            TextField text = (TextField)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveNameClickHandler != null)
                    saveNameClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelNameClickHandler != null)
                    cancelNameClickHandler.handle(editBox);
            });
        });
        editProgressClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced,60);
            TextField text = (TextField)editBox.getChildren().get(0);
            text.setText(text.getText().replace("  /  ", ""));
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            Label lbl = new Label("  /  ");
            lbl.getStyleClass().add("assessment-info");
            editBox.getChildren().add(lbl);
            save.setOnMouseClicked(event -> {
                if(saveProgressClickHandler != null) 
                    saveProgressClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelProgressClickHandler != null)
                    cancelProgressClickHandler.handle(editBox);
            });
        });
        editTargetClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTextField(parent_,toBeReplaced,60);
            TextField text = (TextField)editBox.getChildren().get(0);
            text.setText(text.getText().replace("  " + activity.getUnits(), ""));
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            Label lbl = new Label("  " + activity.getUnits());
            lbl.getStyleClass().add("assessment-info");
            editBox.getChildren().add(1,lbl);
            save.setOnMouseClicked(event -> {
                if(saveTargetClickHandler != null) 
                    saveTargetClickHandler.handle(text);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelTargetClickHandler != null)
                    cancelTargetClickHandler.handle(editBox);
            });
        });
        addTaskClickHandler = ((parent_, toBeReplaced) -> {
            HBox editBox = replaceForTasksComboBox(parent_,toBeReplaced);
            ComboBox combo = (ComboBox)editBox.getChildren().get(0);
            Button save = (Button)editBox.getChildren().get(1);
            Button cancel = (Button)editBox.getChildren().get(2);
            save.setOnMouseClicked(event -> {
                if(saveTaskAddedClickHandler != null) 
                    saveTaskAddedClickHandler.handle(combo);
            });
            cancel.setOnMouseClicked(event -> {
                if(cancelAddTaskClickHandler != null)
                    cancelAddTaskClickHandler.handle(editBox);
            });
        });
        /*********************** END OF CLICK HANDLERS ***********************/
        
        width = parent.getView().widthProperty().multiply(0.80).subtract(60);
        
        panel = new VBox();
        panel.prefWidthProperty().bind(width);
        panel.setMinWidth(500);
        panel.setPadding(new Insets(30,40,30,40));
        panel.setAlignment(Pos.TOP_LEFT);
        
        Region region2 = new Region();
        HBox.setHgrow(region2, Priority.ALWAYS);
        //title
        title = new Label(activity.getName());
        title.getStyleClass().add("assessment-title");
        deleteButton = new Button("Delete");
        deleteButton.getStyleClass().add("assessment-delete-task");
        titleAndDelete = new HBox();
        titleAndDelete.getChildren().addAll(title, region2, deleteButton);
        titleAndDelete.setMaxWidth(700);
        //Type label
        typeLabel = new Label(activity.getType() + " Activity");
        typeLabel.getStyleClass().add("assessment-type");
        //Info boxes
        assessmentInfo = infoBox("Assessment:",activity.getAssessment());
        assessmentInfo.setPadding(new Insets(20,0,0,0));
        assessmentInfo.getChildren().get(1).getStyleClass()
                                    .add("assessment-underline-hover");
        typeInfo = infoBox("Type:",activity.getType());
        typeInfoLbl = (Label)typeInfo.getChildren().get(1);
        progressBarInfo = infoProgressBox("Progress:",activity.getProgress());
        progressInfo = infoBox("",activity.getTarget() + "  " + activity.getUnits());
        progressInfo.setAlignment(Pos.BOTTOM_LEFT);
        targetLbl = (Label)progressInfo.getChildren().get(1);
        progressLbl = new Label(activity.getCompleted() + "  /  ");
        progressLbl.getStyleClass().add("assessment-info");
        progressInfo.getChildren().add(1, progressLbl);
        tasksInfo = tasksBox();
        //Notes
        notesLabelBar = notesLabelBar();
        noteEntries = notesEntries();
        addNote = new HBox();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        addNote.getChildren().addAll(addNoteButton(),region1);
        addNote.setStyle("-fx-padding: 10 0 0 0");
        
        // Setting on mouse click handlers
        assessmentInfo.getChildren().get(1).setOnMouseClicked(event -> {
                if(assessmentClickHandler != null)
                    assessmentClickHandler.handle();
            });
        title.setOnMouseClicked(event -> {
            if(editNameClickHandler != null) editNameClickHandler.handle(titleAndDelete,title);
        });
        deleteButton.setOnAction(event -> {
            if(deleteClickHandler != null) deleteClickHandler.handle();
        });
        addNote.setOnMouseClicked(event -> {
            if(addNoteClickHandler != null) addNoteClickHandler.handle();
        });
        notesLabelBar.getChildren().get(2).setOnMouseClicked(event -> {
            if(addNoteClickHandler != null) addNoteClickHandler.handle();
        });
        progressLbl.setOnMouseClicked(event -> {
            if(editProgressClickHandler != null)
            editProgressClickHandler.handle(progressInfo,progressLbl);
        });
        targetLbl.setOnMouseClicked(event -> {
            if(editTargetClickHandler != null)
            editTargetClickHandler.handle(progressInfo,targetLbl);
        });
        
        panel.getChildren().addAll(titleAndDelete,typeLabel,assessmentInfo, typeInfo,
                progressBarInfo,progressInfo, tasksInfo,notesLabelBar,
                noteEntries,addNote);
        setContent(panel);
    }
    
    // Info Box is a HBox of the format   | Label:     Text   |
    public HBox infoBox(String label, String text){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(140.0);
        Label textBox = new Label(text);
        textBox.setMinWidth(140.0);
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, textBox);
        return hbox;
    }
    
    // InfoProgressBox returns a HBox similar to InfoBox but instead of a text
    // in front of the label it shows a progress bar, followed by the value in %
    public HBox infoProgressBox(String label, float progress){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(140.0);
        int percentage = (int)(progress*100);
        Label textBox = new Label(percentage + "%");
        labelBox.getStyleClass().add("assessment-info");
        textBox.getStyleClass().add("assessment-info");
        textBox.setPadding(new Insets(0,0,0,10));
        hbox.getChildren().addAll(labelBox, DeadlinesPane.createProgressBar(progress),textBox);
        return hbox;
    }
    
    // Similar to InfoBox, but the right side is a VBox listing all the tasks
    // associated with the activity (with additional line to add more tasks)
    public HBox tasksBox(){
        HBox hbox = new HBox();
        Label labelBox = new Label("Tasks:");
        labelBox.setPrefWidth(140.0);
        tasksBox = new VBox();
        tasksBox.setMinWidth(140.0);
        labelBox.getStyleClass().add("assessment-info");
        for(TaskInfo t : activity.getTasks()){
            HBox entry = taskEntry(t);
            tasksBox.getChildren().add(entry);
        }
        addTastLbl = new Label("(+) Associate to another task");
        addTastLbl.getStyleClass().add("assessment-info");
        addTastLbl.getStyleClass().add("assessment-underline-hover");
        addTastLbl.setStyle("-fx-font-size: 12px; ");
        addTastLbl.setOnMouseClicked(event -> {
            if(addTaskClickHandler != null)
            addTaskClickHandler.handle(tasksBox,addTastLbl);
        });
        tasksBox.getChildren().add(addTastLbl);
        hbox.getChildren().addAll(labelBox, tasksBox);
        return hbox;
    }
    // A single entry for tasksBox. It shows the name of the tas (clickable to take 
    // to that task's view), and a X symbol to be remove task
    public HBox taskEntry(TaskInfo t){
        HBox entry = new HBox();
        Label entryLbl = new Label(t.getName());
        entryLbl.getStyleClass().add("assessment-info");
        entryLbl.getStyleClass().add("assessment-underline-hover");
        entryLbl.setPadding(new Insets(0,5,0,0));
        entryLbl.setOnMouseClicked(event -> {
            if(taskClickHandler != null)
                taskClickHandler.handle(t.getName());
        });
        ImageView icon_delete = deleteIcon();
        icon_delete.getStyleClass().add("dashboard-delete-item");
        icon_delete.setOnMouseClicked(event -> {
            if(removeTaskClickHandler != null) removeTaskClickHandler.handle(entry);
        });
        
        entry.setAlignment(Pos.CENTER_LEFT);
        entry.getChildren().addAll(entryLbl,icon_delete);
        return entry;
    }
    
    // Top row for the notes entries, with a title and a button to add new note on the right
    public HBox notesLabelBar(){
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.getStyleClass().add("assessment-tasks-label-bar");
        hbox.setMaxWidth(700);
        Label activitiesLabel = new Label("Notes");
        activitiesLabel.getStyleClass().add("assessment-tasks-label");
        
        HBox addNoteBox = addNoteButton();
        Region region1 = new Region();
        HBox.setHgrow(region1, Priority.ALWAYS);
        
        hbox.getChildren().addAll(activitiesLabel,region1,addNoteBox);
        
        return hbox;
    }
    
    // Button to add a new note
    public HBox addNoteButton(){
        HBox addNoteBox = new HBox();
        addNoteBox.getStyleClass().add("assessment-add-task-box");
        addNoteBox.setAlignment(Pos.CENTER);
                
        ImageView icon = new ImageView();
        icon.getStyleClass().add("dashboard-entry-icon");
        icon.setImage(new Image("icon_add.png"));
        icon.setPreserveRatio(true);
        icon.setFitHeight(24.0);
        
        Label addNoteLabel = new Label("Add new note");
        addNoteLabel.getStyleClass().add("assessment-add-task-label");
        
        addNoteBox.getChildren().addAll(icon,addNoteLabel);
        
        return addNoteBox;
    }
    
    // Note entries are displayed in a Flowpane
    public FlowPane notesEntries(){
        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(10);
        flow.setHgap(10);
        flow.setPrefWrapLength(700); // preferred width allows for two columns
        //flow.setStyle("-fx-background-color: DAE6F3;");
        
        for(NoteInfo n : activity.getNotes()){
            VBox entry = noteEntry(n);
            flow.getChildren().add(entry);
        }
        return flow;
    }
    
    // Each note entry displays the date on top, followed by the note itself
    public VBox noteEntry(NoteInfo n){
        ImageView icon_delete = deleteIcon();
        
        Region reg = new Region();
        HBox.setHgrow(reg, Priority.ALWAYS);
        
        VBox entry = new VBox();
        entry.getStyleClass().add("assessment-note-entry");
        entry.setPrefWidth(225);
        entry.setMaxWidth(225);
        Label dateLabel = new Label(n.getDate());
        dateLabel.getStyleClass().add("assessment-task-entry-text");
        dateLabel.setStyle("-fx-padding: 0 0 5 0");
        Text noteText = new Text(n.getNote());
        noteText.setWrappingWidth(205);
        noteText.getStyleClass().add("assessment-task-entry-text");
        entry.getChildren().addAll(new HBox(dateLabel, reg,icon_delete), noteText);
        
        icon_delete.setOnMouseClicked(event -> {
            if(deleteNoteClickHandler != null) deleteNoteClickHandler.handle(entry);
        });
        
        return entry;
    }
    // Method to add a note, and displaying it on the view
    public void addNoteEntry(String note){
        NoteInfo noteInfo = controller.addNote(note);
        
        if(noteInfo!=null){
            activity.addNote(noteInfo);
            VBox entry = noteEntry(noteInfo);

            noteEntries.getChildren().add(entry);
        }
    }
    // Method to remove a note, and removing it from the view
    private void removeNoteEntry(VBox entry, String note) {
        for(NoteInfo n : activity.getNotes()){
            if(n.getNote().equals(note)){
                activity.getNotes().remove(n);
                FlowPane parent = (FlowPane)entry.getParent();
                parent.getChildren().remove(entry);
            }
        }
    }
    // Method to remove a task, removing the respective entry from the list
    private void removeTaskEntry(HBox entry, String taskName_) {
        for(TaskInfo t : activity.getTasks()){
            if(t.getName().equals(taskName_)){
                activity.getTasks().remove(t);
                VBox parent = (VBox)entry.getParent();
                parent.getChildren().remove(entry);
            }
        }
    }
    /* Replaces an element parsed as argument for a TextField to allow user input
     * so an user can edit a certain field */
    public HBox replaceForTextField(Pane parent, Node toReplace, float width){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        // Setting the text of the TextField as the text the label replaced
        if (toReplace instanceof Label) {
            Label lbl = (Label)toReplace;
            TextField text = new TextField(lbl.getText());
            box.getChildren().add(text); 
            text.setPrefWidth(width);
            if(lbl.getText()!=null && lbl.getText().equals(activity.getName())){
                text.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
            }
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    // replaceForTextField with a default width of 500px
    public HBox replaceForTextField(Pane parent, Node toReplace){
        return replaceForTextField(parent, toReplace, 500f);
    }
    /* Replaces an element parsed as argument for a ComboBox to allow user input
     * so an user can choose from limit set of options (for adding tasks) */
    public HBox replaceForTasksComboBox(Pane parent, Node toReplace){
        // Checking the position of the element to be replaced in its parent's children
        int position = 0;
        for (int i = 0; i<parent.getChildren().size(); i++) {
            if (parent.getChildren().get(i).equals(toReplace)) {
                position = i;
                break;
            }
        }
        HBox box = new HBox();
        box.setSpacing(5);
        if (toReplace instanceof Label) {
            ComboBox optionBox = new ComboBox();
            for(String option : controller.getTaskOptions()){
                optionBox.getItems().add(option);
            }
            box.getChildren().add(optionBox); 
            optionBox.setPrefWidth(400);
        }
        // Save and cancel buttons
        Button save = new Button("Save changes");
        save.getStyleClass().add("assessment-save-button");
        Button cancel = new Button("Cancel");
        cancel.getStyleClass().add("assessment-cancel-button");
        box.setAlignment(Pos.BOTTOM_LEFT);
        box.getChildren().addAll(save,cancel); 
        parent.getChildren().remove(toReplace);
        parent.getChildren().add(position, box);
        return box;
    }
    
    public interface AssessmentClickHandler{
        void handle();
    }
    public interface TaskClickHandler{
        void handle(String TaskName);
    }
    public interface DeleteClickHandler{
        void handle();
    }
    public interface EditClickHandler{
        void handle(Pane parent, Node toReplace);
    }
    public interface CancelClickHandler{
        void handle(HBox editBox);
    }
    public interface SaveTxtFieldClickHandler{
        void handle(TextField newInfo);
    }
    public interface DeleteNoteClickHandler{
        void handle(VBox entry);
    }
    public interface RemoveTaskClickHandler{
        void handle(HBox entry);
    }
    public interface AddNoteClickHandler{
        void handle();
    }
    public interface SaveComboClickHandler{
        void handle(ComboBox newInfo);
    }
    public void setActivity(ActivityInfo a){
        this.activity = a;
    }
    
    public static ImageView deleteIcon(){
        ImageView icon_delete = new ImageView();
        icon_delete.setImage(new Image("icon_delete.png"));
        icon_delete.setPreserveRatio(true);
        icon_delete.setFitHeight(12.0);
        icon_delete.setStyle("");
        icon_delete.setOpacity(0.5f);
        
        icon_delete.setOnMouseEntered((MouseEvent t) -> {
            icon_delete.setOpacity(1.0f);
        });

        icon_delete.setOnMouseExited((MouseEvent t) -> {
            icon_delete.setOpacity(0.5f);
        });
        return icon_delete;
    }
}
