/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 * @author heb16acu
 */

import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class CreateTask extends GridPane {
    private TextField taskNameTxt;
    private TextField duration;
    private ComboBox modules;
    private ComboBox assessment;
    private ComboBox dependances;
    private TextArea description;
    private String moduleChange;
    private String assessChange;
    private Runnable onClickModule;
    private Runnable onClickAssess;
    private Runnable onSubmit;

    public CreateTask() {

        getStylesheets().add(SceneManager.class.getResource("style/CreateTask.css").toExternalForm());


        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);

        Label taskName = new Label("Task Name *");
        this.add(taskName, 0, 0);
        taskNameTxt = new TextField();
        this.add(taskNameTxt, 0, 1);


        Label durationLabel = new Label("Duration(days) *");
        this.add(durationLabel, 0, 2);
        duration = new TextField();
        this.add(duration, 0, 3);
        duration.textProperty().addListener((o, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                duration.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        Label modulesLabel = new Label("Modules *");
        this.add(modulesLabel, 0, 4);
        modules = new ComboBox();
        this.add(modules, 0, 5);

        Label Assessment = new Label("Assessment *");
        this.add(Assessment, 1, 4);
        assessment = new ComboBox();
        this.add(assessment, 1, 5);

        Label dependancesLabel = new Label("Dependances");
        this.add(dependancesLabel, 0, 6);
        dependances = new ComboBox();
        this.add(dependances, 0, 7);


        Label taskDescription = new Label("Task description");
        this.add(taskDescription, 0, 8);
        description = new TextArea();
        this.description.setPrefColumnCount(20);
        this.description.setPrefRowCount(5);
        this.description.setWrapText(true);
        this.add(description, 0, 9);


        Button submitButton = new Button("Submit");
        this.add(submitButton, 0, 11);

        Text submitText = new Text("Task successfully created");
        //loggedIn.setFont(myFontButton);
        submitText.setFill(Color.GREEN);
        submitText.setVisible(false);
        this.add(submitText, 0, 12);

        Text failedSubmitText = new Text("Invalid field");
        //loggedIn.setFont(myFontButton);
        failedSubmitText.setFill(Color.RED);
        failedSubmitText.setVisible(false);
        this.add(failedSubmitText, 0, 12);

        Text endDateText = new Text("Incorrect end date");
        //loggedIn.setFont(myFontButton);
        endDateText.setFill(Color.RED);
        endDateText.setVisible(false);
        this.add(endDateText, 0, 12);


        this.assessment.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> ov,
                                final String oldvalue, final String newvalue) {
                assessChange = newvalue;
                if (onClickAssess != null) onClickAssess.run();
            }
        });

        this.modules.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> ov,
                                final String oldvalue, final String newvalue) {
                moduleChange = newvalue;
                if (onClickModule != null) onClickModule.run();
            }
        });

        submitButton.setOnAction(e -> {
            if (taskNameTxt.getText().isEmpty() ||

                    assessment.getSelectionModel().isEmpty()) {
                failedSubmitText.setVisible(true);
                submitText.setVisible(false);
                endDateText.setVisible(false);
            } else {
                if (onSubmit != null) onSubmit.run();
                submitText.setVisible(true);

                failedSubmitText.setVisible(false);
                endDateText.setVisible(false);
                taskNameTxt.clear();
                duration.clear();
                modules.getSelectionModel().clearSelection();
                assessment.getSelectionModel().clearSelection();
                dependances.getSelectionModel().clearSelection();
                description.clear();


            }


        });

    }


    public void setOnSubmit(Runnable r) {
        this.onSubmit = r;
    }

    public void setOnClickModule(Runnable r) {
        this.onClickModule = r;
    }

    public void setOnClickAssess(Runnable r) {
        this.onClickAssess = r;
    }

    public String getModuleChange() {
        return this.moduleChange;
    }

    public String getAssessChange() {
        return this.assessChange;
    }

    public TextField getTaskNameTxt() {
        return this.taskNameTxt;
    }


    public TextField getDuration() {
        return this.duration;
    }


    public ComboBox getModule() {

        return this.modules;
    }


    public TextArea getDescription() {
        return this.description;
    }


    public void setModules(ArrayList<String> modules) {

        ObservableList<String> list = FXCollections.observableArrayList(modules);

        this.modules.setItems(list);

    }

    public void setAssessments(ArrayList<String> assessment) {

        ObservableList<String> list = FXCollections.observableArrayList(assessment);

        this.assessment.setItems(list);

    }

    public void setDependances(ArrayList<String> tasks) {

        ObservableList<String> list = FXCollections.observableArrayList(tasks);

        this.dependances.setItems(list);

    }

    public ComboBox getDependances() {
        return this.dependances;
    }

    public void setAssessmentPrefilled(String module, String assessment_) {
        modules.setValue(module);
        modules.setDisable(true);
        //this.getChildren().remove(modules);
        //this.add(new Label(module),1,5);

        assessment.setValue(assessment_);
        assessment.setDisable(true);
        //this.getChildren().remove(assessment);
        //this.add(new Label(assessment_),0,7);

    }
}

  
