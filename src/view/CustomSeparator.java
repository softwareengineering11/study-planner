package view;

import javafx.scene.control.Separator;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;

public class CustomSeparator extends Separator {

    public CustomSeparator() {
        Ellipse separatorClip = new Ellipse();
        separatorClip.centerXProperty().bind(widthProperty().multiply(0.5));
        separatorClip.centerYProperty().bind(heightProperty().multiply(0.5));
        separatorClip.radiusXProperty().bind(widthProperty().multiply(0.4));
        separatorClip.radiusYProperty().bind(heightProperty().multiply(0.5));
        setClip(separatorClip);
    }
}
