package view;

//import com.sun.xml.internal.ws.api.model.wsdl.editable.EditableWSDLBoundFault;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LoginForm extends GridPane {

    private EventHandler<ActionEvent> registerHandler;
    private EventHandler<ActionEvent> loginHandler;
    private EventHandler<ActionEvent> succesLoginHandler;

    private StringProperty cantLogin;
    private StringProperty email;
    private StringProperty pw;

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public String getPw() {
        return pw.get();
    }

    public StringProperty pwProperty() {
        return pw;
    }

    public LoginForm() {
        this.getStyleClass().add("container");
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setMaxHeight(480.0);
        this.setMaxWidth(480.0);

        email = new SimpleStringProperty();
        pw = new SimpleStringProperty();
        cantLogin = new SimpleStringProperty();

        this.getStylesheets().add(SceneManager.class.getResource("style/LoginForm.css").toExternalForm());
        //this.setPadding(new Insets(25,25,25,25));
        Font labelFont = null;
        Font welcomeFont = null;
        try {
            labelFont = Font.loadFont(new FileInputStream("res/ProxNova.ttf"), 24);
            welcomeFont = Font.loadFont(new FileInputStream("res/Font2.ttf"), 42);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (labelFont == null) labelFont = Font.font(24);

        else if (welcomeFont == null) welcomeFont = Font.font(36);
        Font buttonFont = Font.font(labelFont.getFamily(), 14);


        Text headerText = new Text("Welcome");

        //Welcome
        headerText.setFont(welcomeFont);
        headerText.setFill(Color.WHITE);
        this.add(headerText, 0, 0, 2, 1);
        this.setMargin(headerText, new Insets(0, 0, 35, 60));


        TextField emailBox = new TextField();
        emailBox.setPromptText("Username");
        emailBox.setFont(labelFont);

        this.add(emailBox, 0, 1, 2, 1);


        PasswordField passwordBox = new PasswordField();
        passwordBox.setPromptText("Password");

        passwordBox.setFont(Font.font(24));
        //passwordBox.setStyle("-fx-text-fill:black;");
        this.add(passwordBox, 0, 3, 2, 1);
        this.setMargin(passwordBox, new Insets(-10, 0, 0, 0));


        // SUBMIT and REGISTER VBOX
        Button loginButton = new Button("Log in");
        loginButton.setFont(buttonFont);
        VBox formControls = new VBox(10);
        formControls.setAlignment(Pos.BOTTOM_CENTER);
        formControls.getChildren().add(loginButton);


        Button register = new Button("Register");
        register.getStyleClass().add("register");
        register.underlineProperty();
        register.setFont(buttonFont);
        register.setTextFill(Color.WHITESMOKE);
        //register.setStyle("-fx-border-color:transparent;");
        register.setUnderline(true);

        formControls.getChildren().add(register);

        Label unable = new Label("");
        unable.setFont(buttonFont);
        unable.setStyle("-fx-text-fill:red;");
        formControls.getChildren().add(unable);

        this.add(formControls,1,7);








        // UI Events
        loginButton.setOnMouseEntered(event -> loginButton.setStyle("-fx-text-fill:blue;"));

        loginButton.setOnMouseExited(event -> loginButton.setStyle("-fx-background-color:transparent;"));

        loginButton.setOnAction(event -> {
            if(loginHandler != null) {
                loginHandler.handle(event);
                return;
            }
            //if unable to login do this
            //unable.setStyle("-fx-text-fill:red;");
        });

        register.setOnMouseEntered(event -> register.setStyle("-fx-text-fill:blue;"));

        register.setOnMouseExited(event -> register.setStyle("-fx-text-fill:white;"));

        register.setOnAction(event -> {
            if(registerHandler != null) registerHandler.handle(event);
        });

        email.bindBidirectional(emailBox.textProperty());
        pw.bindBidirectional(passwordBox.textProperty());
        cantLogin.bindBidirectional(unable.textProperty());
    }


    public void unableToLogin(){
        cantLogin.setValue("Incorrect Email/Password");
    }
    public void setEmpty(){
        email.setValue("");
        pw.setValue("");
        cantLogin.setValue("");
    }

    public void setRegisterHandler(EventHandler<ActionEvent> registerHandler) {
        this.registerHandler = registerHandler;
    }


    public void setLoginHandler(EventHandler<ActionEvent> loginHandler) {
        this.loginHandler = loginHandler;
    }


}
