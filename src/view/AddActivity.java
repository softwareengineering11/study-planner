package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import view.assessment.TaskInfo.ActivityInfo;

/**
 * Add Activity pop up window
 * 
 */
public class AddActivity {
    static ActivityInfo activity = null;
    
    public static ActivityInfo display(String taskName){
        Stage window = new Stage();
        
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add new note");
        window.setMinWidth(250);
        
        Label title = new Label("Add new activity");
        title.setStyle("-fx-font-size: 20px; -fx-font-weight: bold;");
        HBox taskBox = infoBox("Task:",new Label(taskName));
        TextField name = new TextField();
        name.setPrefWidth(400);
        HBox nameBox = infoBox("Name:",name);
        
        ComboBox typeComboBox = new ComboBox();
        for(String type : ActivityInfo.getTypes().keySet()){
            typeComboBox.getItems().add(type);
        }
        typeComboBox.setPrefWidth(150);
        HBox typeBox = infoBox("Type:",typeComboBox);
        
        TextField target = new TextField();
        target.setPrefWidth(80);
        HBox targetBox = infoBox("Target:",target);
        Label unitsLbl = new Label();
        targetBox.getChildren().add(unitsLbl);
        targetBox.getStyleClass().add("assessment-info");
        
        typeComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                unitsLbl.setText(ActivityInfo.getTypes().get(newValue));
            }
        });
        
        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        
        saveButton.setOnAction(e -> {
            int targetInt;
            if(name.getText()==null || name.getText().isEmpty()){
                AlertBox.display("Invalid Entry", "You must enter a name for the activity.");
            }
            else if(typeComboBox.getValue()==null){
                AlertBox.display("Invalid Entry", "You must select a type of activity.");
            }
            else if(target.getText()==null || target.getText().isEmpty()){
                AlertBox.display("Invalid Entry", "You must must enter a target number.");
            }
            else{ 
                try { 
                    targetInt = Integer.parseInt(target.getText());
                    activity = new ActivityInfo(name.getText(),typeComboBox.getValue().toString(),
                        0.0f,targetInt,0,unitsLbl.getText());
                    window.close();
                }
                catch (NumberFormatException ex) {
                   AlertBox.display("Invalid entry","You must enter a number for target.");
                }
            }
            
        });
        
        cancelButton.setOnAction(e -> {
            activity = null;
            window.close();
        });
        
        HBox buttons = new HBox();
        buttons.getChildren().addAll(saveButton, cancelButton);
        buttons.setSpacing(15);
        buttons.setAlignment(Pos.CENTER_LEFT);
        
        VBox layout = new VBox();
        layout.setPadding(new Insets(15,20,15,20));
        layout.setSpacing(15);
        layout.getChildren().addAll(title,taskBox, nameBox, typeBox,targetBox,buttons);
        layout.setAlignment(Pos.CENTER_LEFT);
        
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        
        return activity;
    }
    
    public static HBox infoBox(String label, Node info){
        HBox hbox = new HBox();
        Label labelBox = new Label(label);
        labelBox.setPrefWidth(100.0);
        labelBox.getStyleClass().add("assessment-info");
        info.getStyleClass().add("assessment-info");
        hbox.getChildren().addAll(labelBox, info);
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.setSpacing(5);
        return hbox;
    }
}
