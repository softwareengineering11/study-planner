package view.ganttchart;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GanttChartTask {

    private GanttChartGroup group;

    private String name;
    private Duration duration;

    private List<GanttChartTask> dependencies;

    public GanttChartTask(String name, Duration duration){
        this.name = name;
        this.duration = duration;
        dependencies = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public Duration getDuration() {
        return duration;
    }

    public List<GanttChartTask> getDependencies() {
        return dependencies;
    }

    public void addDependencies(GanttChartTask... tasks){
        dependencies.addAll(Arrays.asList(tasks));
    }

    public GanttChartGroup getGroup() {
        return group;
    }

    public void setGroup(GanttChartGroup group) {
        this.group = group;
    }

    public int getIndex() {
        return group.getTasks().indexOf(this);
    }
}
