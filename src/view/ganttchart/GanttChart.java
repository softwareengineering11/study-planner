package view.ganttchart;

import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.IntStream;

public class GanttChart extends StackPane {

    private static final double GRID_INSET_X = 150.0;
    private static final double GRID_INSET_Y = 80.0;
    private static final double GRID_GAP = 100.0;
    private static final double GAP_HOURS = 24.0;
    private static final Paint GRID_STROKE_FILL = Color.LIGHTSLATEGREY;
    private static final Paint DATETIME_FILL = Color.BLACK;
    private static final Paint DATETIME_BACKGROUND = Color.grayRgb(240);
    private static final double DATETIME_MONTH_INSET = 40.0;
    private static final Paint GROUP_BACKGROUND = Color.grayRgb(240);
    private static final Paint TASK_BACKGROUND = Color.grayRgb(250);
    private static final double GROUP_NAME_HEIGHT = 64.0;
    private static final Paint GROUP_NAME_FILL = Color.BLACK;
    private static final double TASK_HEIGHT = 24.0;
    private static final double GROUP_TEXT_INSET = 10;
    private static final Paint TASK_NAME_FILL = Color.BLACK;
    private static final Paint TASK_BORDER_FILL = GROUP_BACKGROUND;
    private static final Paint DEPENDENCY_LINE_FILL = Color.SLATEBLUE;

    private Canvas canvas;
    private GraphicsContext graphics;

    private Image groupCache;

    private LocalDateTime startDate;
    private Duration dateOffset;

    private List<GanttChartGroup> groups;

    public GanttChart() {
        groups = new ArrayList<>();

        ScrollPane canvasScroll = new ScrollPane();

        startDate = LocalDateTime.now();
        dateOffset = Duration.ZERO;

        canvas = new Canvas();

        AtomicReference<Point2D> mousePress = new AtomicReference<>(new Point2D(0, 0));

        canvas.setOnMousePressed(event -> mousePress.set(new Point2D(event.getX(), event.getY())));

        canvas.setOnMouseDragged(event -> {
            Point2D delta = new Point2D(event.getX() - mousePress.get().getX(), event.getY() - mousePress.get().getY());
            dateOffset = dateOffset.plusHours((long) (delta.getX()));
            mousePress.set(new Point2D(event.getX(), event.getY()));
            requestRender();
        });

        graphics = canvas.getGraphicsContext2D();

        render();

        canvasScroll.setContent(canvas);
        getChildren().add(canvasScroll);
    }

    private void fillBackground() {
        graphics.setFill(Color.WHITE);
        graphics.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    /**
     * Renders the gantt chart grid with a given offset.
     * @param insetX
     * @param insetY
     */
    private void renderGrid(double insetX, double insetY) {
        graphics.setStroke(GRID_STROKE_FILL);
        graphics.strokeRect(insetX, insetY, canvas.getWidth(), canvas.getHeight());

        for (int i = 0; i < getWidth() / GRID_GAP; ++i) {
            double gx = i * GRID_GAP + insetX;
            graphics.strokeLine(gx, insetY, gx, canvas.getHeight());
        }
    }

    /**
     * Renders the dates above the gantt chart.
     * @param insetX
     */
    private void renderDateTimes(double insetX) {
        graphics.setFill(DATETIME_BACKGROUND);
        graphics.fillRect(insetX, 0, getWidth(), GRID_INSET_Y - 2);

        graphics.setFill(DATETIME_FILL);

        // Render days
        Month prevMonth = null;
        for (double startX = insetX, day = 0; startX < getWidth(); startX += GRID_GAP, ++day) {
            LocalDateTime date = startDate.minusHours(dateOffset.toHours()).plusDays((long) day);
            graphics.fillText(date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.UK), startX + 5, GRID_INSET_Y - 20);
            graphics.fillText(String.valueOf(date.getDayOfMonth()), startX + 5, GRID_INSET_Y - 5);

            // Diplay the month if the month has changed
            if (!date.getMonth().equals(prevMonth)) {
                graphics.fillText(date.getMonth().name() + " (" + date.getYear() + ")", startX + 5, DATETIME_MONTH_INSET);
                prevMonth = date.getMonth();
            }
        }
    }

    /**
     * Measures the size of a given string.
     * @param text
     * @return
     */
    private Bounds measureText(String text) {
        Text textObj = new Text(text);
        textObj.setFont(graphics.getFont());
        Bounds tb = textObj.getBoundsInLocal();
        Rectangle stencil = new Rectangle(tb.getMinX(), tb.getMinY(), tb.getWidth(), tb.getHeight());
        Shape intersection = Shape.intersect(textObj, stencil);
        return intersection.getBoundsInLocal();
    }

    /**
     * Renders text which wrap to the next line if the line exceeds the given max width;
     * @param g
     * @param text
     * @param x
     * @param y
     * @param maxWidth
     */
    private void renderMultilineText(GraphicsContext g, String text, double x, double y, double maxWidth) {
        double textY = 0.0;
        double yPadding = 4.0;

        String[] words = text.split(" ");

        StringBuilder lineBuilder = new StringBuilder();
        for (int i = 0; i < words.length; ++i) {
            lineBuilder.append(words[i]).append(" ");
            Bounds textMetrics = measureText(lineBuilder.toString());
            String nextWord = "";
            if (i < words.length - 1) nextWord = words[i + 1];
            // If the line width exceeds, go to next line.
            if (textMetrics.getWidth() + measureText(nextWord).getWidth() >= maxWidth || i >= words.length - 1) {
                g.fillText(lineBuilder.toString(), x, y + textY);
                lineBuilder = new StringBuilder();
                textY += textMetrics.getHeight() + yPadding;
            }
        }
    }

    /**
     * Renders the gantt chart tasks on the chart.
     * @param g
     * @param task
     * @param taskY
     */
    private void renderGroupTask(GraphicsContext g, GanttChartTask task, double taskY) {
        // Render task background
        g.setFill(TASK_BACKGROUND);
        g.fillRect(0, taskY, GRID_INSET_X - 2, TASK_HEIGHT);

        // Render task text
        g.setFill(TASK_NAME_FILL);
        renderMultilineText(g, task.getName(), GROUP_TEXT_INSET, taskY + TASK_HEIGHT * 0.66, GRID_INSET_X - 2);

        // Render Top Border
        g.setStroke(TASK_BORDER_FILL);
        g.strokeLine(0, taskY, GRID_INSET_X - 2, taskY);

    }

    /**
     * Calculates the height of a group including the tasks and group name.
     * @param group
     * @return
     */
    private double getGroupHeight(GanttChartGroup group) {
        return GROUP_NAME_HEIGHT + group.getTasks().stream().reduce(0.0, (accum, task) -> accum + TASK_HEIGHT, (left, right) -> left + right);
    }

    /**
     * Calculates the on screen width of a given duration.
     * @param duration
     * @return
     */
    private double getDurationWidth(Duration duration) {
        return (duration.toMinutes() / 60.0 / GAP_HOURS) * GRID_GAP;
    }

    /**
     * Calculates the y coordinates of a group.
     * @param group
     * @return
     */
    private double getGroupY(GanttChartGroup group) {
        int groupIndex = groups.indexOf(group);
        double groupY = GRID_INSET_Y;
        for (int i = 0; i < groupIndex; ++i) {
            groupY += getGroupHeight(groups.get(i));
        }
        return groupY;
    }

    /**
     * Calculates the y coordinates of a task.
     * @param task
     * @return
     */
    private double getTaskY(GanttChartTask task) {
        double groupY = getGroupY(task.getGroup());
        int taskIndex = task.getIndex();
        return groupY + taskIndex * TASK_HEIGHT + GROUP_NAME_HEIGHT;
    }

    /**
     * Finds the offset of the date offset.
     * @return
     */
    private double getStartDateOffsetX(){
        return dateOffset.toHours() * (GRID_GAP / GAP_HOURS);
    }

    private double _floorMultiple(double value, double floor){
        return (int)(value / floor) * floor;
    }

    private double getTaskX(GanttChartTask task) {
        return task.getGroup().getTasks().stream().limit(task.getIndex()).reduce(0.0, (accum, t) -> accum + getDurationWidth(t.getDuration()), (a, b) -> a + b) + GRID_INSET_X + 2 + _floorMultiple(getStartDateOffsetX() + 2, GRID_GAP);
    }

    /**
     * Renders the group and task names on the left of the gantt chart.
     * @param g
     */
    private void _renderGroups(GraphicsContext g) {
        IntStream.range(0, groups.size()).forEach(groupIndex -> {
            GanttChartGroup group = groups.get(groupIndex);

            double groupY = getGroupY(group);

            // Render group background
            g.setFill(GROUP_BACKGROUND);
            g.fillRect(0, groupY, GRID_INSET_X - 2, GROUP_NAME_HEIGHT);

            // Render group name
            g.setFill(GROUP_NAME_FILL);
            renderMultilineText(g, group.getName(), GROUP_TEXT_INSET, groupY + g.getFont().getSize(), GRID_INSET_X - 12);

            // Render group tasks

            IntStream.range(0, group.getTasks().size()).forEach(index -> {
                GanttChartTask task = group.getTasks().get(index);
                renderGroupTask(g, task, groupY + GROUP_NAME_HEIGHT + (index * TASK_HEIGHT));
            });

        });
    }

    public void invalidateCache(){
        groupCache = null;
    }

    private void renderGroupCache() {
        if (groups.size() < 1) return;
        Canvas temp = new Canvas(GRID_INSET_X, getGroupY(groups.get(groups.size() - 1)) + getGroupHeight(groups.get(groups.size() - 1)));
        GraphicsContext g = temp.getGraphicsContext2D();

        _renderGroups(g);

        groupCache = temp.snapshot(null, null);
    }

    private void renderGroups() {
        if (groupCache == null) {
            renderGroupCache();
        }
        graphics.drawImage(groupCache, 0, 0);
    }

    private double getGroupOffset(GanttChartGroup group){
        return _floorMultiple(getDurationWidth(Duration.between(startDate, group.getStart())), GRID_GAP);
    }

    private void renderTask(GanttChartTask task) {
        Stop[] fillStops = new Stop[]{
                new Stop(0, Color.rgb(170, 250, 218)),
                new Stop(1, Color.rgb(119, 199, 196))
        };
        LinearGradient taskFill = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, fillStops);
        graphics.setFill(taskFill);
        graphics.fillRoundRect(getTaskX(task) + getGroupOffset(task.getGroup()), getTaskY(task), getDurationWidth(task.getDuration()), TASK_HEIGHT, 5, 5);
    }

    /**
     * Renders the dependency arrows
     * @param task
     */
    private void renderDependencies(GanttChartTask task) {
        graphics.setStroke(DEPENDENCY_LINE_FILL);
        graphics.setFill(DEPENDENCY_LINE_FILL);
        task.getDependencies().forEach(dependency -> {
            double dY = getTaskY(dependency) + TASK_HEIGHT * 0.5;
            double dX = getTaskX(dependency) + getDurationWidth(dependency.getDuration()) + getGroupOffset(task.getGroup());
            double tX = getTaskX(task) + 10.0 + getGroupOffset(task.getGroup());
            double tY = getTaskY(task);

            graphics.strokeLine(dX, dY, tX - 5.0, dY);
            graphics.strokeLine(tX, dY + 5.0, tX, tY);
            graphics.beginPath();
            graphics.arc(tX - 5.0, dY + 5.0, 5.0, 5.0, 0.0, 90.0);
            graphics.stroke();

            // Arrow
            double[] xPoints = {
                    dX, dX + 5, dX + 5
            };
            double[] yPoints = {
                    dY, dY + 5.0, dY - 5.0
            };
            graphics.fillPolygon(xPoints, yPoints, xPoints.length);
        });
    }

    private void renderGanttChart() {
        groups.forEach(group -> group.getTasks().forEach(task -> {
            renderTask(task);
            renderDependencies(task);
        }));
    }

    private void render() {
        if (groups.size() > 0) canvas.setHeight(getGroupHeight(groups.get(groups.size() - 1)) + getGroupY(groups.get(groups.size() - 1)));
        fillBackground();

        renderGrid(GRID_INSET_X, GRID_INSET_Y);
        renderDateTimes(GRID_INSET_X);

        renderGanttChart();
        renderGroups();

    }

    public void requestRender() {
        render();
    }

    public void addGroup(GanttChartGroup group) {
        this.groups.add(group);
    }

    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        canvas.setLayoutX(snappedLeftInset());
        canvas.setLayoutY(snappedTopInset());
        canvas.setWidth(snapSize(getWidth()) - snappedLeftInset() - snappedRightInset());
        canvas.setHeight(snapSize(getHeight()) - snappedTopInset() - snappedBottomInset());
        requestRender();
    }

    public void clearGroups() {
        this.groups.clear();
    }
}
