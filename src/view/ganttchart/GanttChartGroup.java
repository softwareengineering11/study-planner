package view.ganttchart;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GanttChartGroup {

    private String name;
    private List<GanttChartTask> tasks;

    private LocalDateTime start;

    public GanttChartGroup(String name, LocalDateTime startDateTime) {
        this.name = name;
        tasks = new ArrayList<>();
        this.start = startDateTime;
    }

    public String getName() {
        return name;
    }

    public List<GanttChartTask> getTasks() {
        return tasks;
    }

    public void addTasks(GanttChartTask... task){
        tasks.addAll(Arrays.asList(task));
        tasks.forEach(t -> t.setGroup(this));
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void addDependency(GanttChartTask ganttTask, GanttChartTask dependencyTask) {
        System.out.println(tasks.indexOf(ganttTask));
        tasks.get(tasks.indexOf(ganttTask)).addDependencies(tasks.get(tasks.indexOf(dependencyTask)));
    }
}
