package view;

import javafx.scene.Scene;

import java.util.HashMap;

public class SceneManager {

    private HashMap<String, Scene> scenes;

    private SceneChangeListener onSceneChange;

    private String currentScene;

    public SceneManager(){
        currentScene = "";
        scenes = new HashMap<>();
    }

    public void add(String name, Scene scene){
        if(scenes.containsKey(name)) scenes.remove(name);
        scenes.put(name, scene);
    }

    public void setOnSceneChange(SceneChangeListener r){
        this.onSceneChange = r;
    }

    public void changeScene(String name){
        if(!scenes.containsKey(name)) throw new RuntimeException("Scene " + name + " does not exist!");
        if(currentScene.equals(name)) throw new RuntimeException("Scene " + name + " is already displayed!");
        currentScene = name;
        if(onSceneChange == null){
            System.out.println("Warning: SceneManager scene change listener is not set.");
            return;
        }
        Scene scene = scenes.get(name);
        scene.getStylesheets().addAll(getClass().getResource("style/common.css").toExternalForm());
        onSceneChange.change(scenes.get(name));
    }

    interface SceneChangeListener{
        void change(Scene scene);
    }

}
